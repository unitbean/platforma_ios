//
//  ChoosenEventsChoosenEventsProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit

/// ChoosenEvents DataProvider Interface
protocol ChoosenEventsDataProviderInput: DataProviderStoreGetter {
    func getEvents(completion: @escaping ([EventModel]?, String?) -> Void)
    func storeChoosenEvents(events: ChoosenEventsModel?)
}

/// ChoosenEvents Router Interface
protocol ChoosenEventsRouterProtocol {
    func routeToDetailedEvent(fromVc: ChoosenEventsViewController, modelIndex: Int)
    func routeToSubPurchase(fromVc: ChoosenEventsViewController)
}

/// ChoosenEvents ViewController Interface
protocol ChoosenEventsViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayChoosenEvents(viewModel: ChoosenEvents.ShowEvents.ViewModel)
}

/// ChoosenEvents View Interface
protocol ChoosenEventsViewInput: class {
    func displayNoResult()
    func hideNoResult()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateCollectionViewData()
}

/// ChoosenEvents Interactor Interface
protocol ChoosenEventsInteractorInput {
    func loadChoosenEvents(request: ChoosenEvents.ShowEvents.Request)
}

/// ChoosenEvents Presenter Interface
protocol ChoosenEventsPresenterInput: class {
    func presentChoosenEvents(response: ChoosenEvents.ShowEvents.Response)
}






