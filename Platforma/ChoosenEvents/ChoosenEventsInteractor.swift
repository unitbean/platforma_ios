//
//  ChoosenEventsChoosenEventsInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// Класс для описания бизнес-логики модуля ChoosenEvents
class ChoosenEventsInteractor: ChoosenEventsInteractorInput {
    
    let presenter: ChoosenEventsPresenterInput
    let dataProvider: ChoosenEventsDataProviderInput
    
    let worker = ChoosenEventsInteractorWorker()

    required init(dataProvider: ChoosenEventsDataProviderInput, presenter: ChoosenEventsPresenterInput) {
        self.dataProvider = dataProvider
        self.presenter = presenter
    }

    func loadChoosenEvents(request: ChoosenEvents.ShowEvents.Request) {
        dataProvider.getEvents { [unowned self] (events, error) in
            let result: ChoosenEvents.ShowEvents.RequestResult
            
            let filteredEvents = self.worker.filterChoosenEvents(events: events)
            
            if self.isEqualToStoredModels(models: filteredEvents?.events) {
                return
            } else {
                self.dataProvider.storeChoosenEvents(events: filteredEvents)
                
                if let choosenEvents = filteredEvents {
                    result = .success(choosenEvents)
                } else if let error = error {
                    result = .failure(error)
                } else {
                    result = .failure("Ошибка получения данных")
                }
                self.presenter.presentChoosenEvents(response: ChoosenEvents.ShowEvents.Response(result: result))
            }
        }
    }
}

//MARK: - InteractorModelComparator
extension ChoosenEventsInteractor: InteractorModelComparator {
    func isEqualToStoredModels<T: Equatable>(models: [T]?) -> Bool {
        return models == dataProvider.getStoredData()
    }
}
