//
//  ChoosenEventsInteractorWorker.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 07.01.2019.
//  Copyright © 2019 UnitBean. All rights reserved.
//

import Foundation

class ChoosenEventsInteractorWorker {
    func filterChoosenEvents(events: [EventModel]?) -> ChoosenEventsModel? {
        guard let events = events, let choosenEventsIds = PlatformaUserDefaults.getChoosenEventsIds() else { return nil }
        let filteredEvents: [EventModel] = events.filter{choosenEventsIds.contains($0.id!)}
        return ChoosenEventsModel(events: filteredEvents)
    }
}
