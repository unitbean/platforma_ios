//
//  ChoosenEventCollectionViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 07.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class ChoosenEventCollectionViewCell: UICollectionViewCell, CardCollectionViewCellProtocol {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    var viewModel: EventViewModel? {
        didSet {
            typeLabel.text = viewModel?.type
            titleLabel.text = viewModel?.title
            if PlatformaUserDefaults.getUserPremiumStatus() == true {
                priceLabel.text = "FREE"
                priceLabel.textColor = UIColor.white
                priceView.backgroundColor = ColorConstants.pink
                dateTimeLabel.text = viewModel?.startDate
                placeLabel.text = viewModel?.placeTitle
            } else if let price = viewModel?.price {
                priceLabel.text = String(describing: price) + " ₽"
                priceLabel.textColor = UIColor.black
                priceView.backgroundColor = ColorConstants.cyan
                dateTimeLabel.text = viewModel?.startDate
                placeLabel.text = viewModel?.placeTitle
            } else {
                priceLabel.text = ""
            }
            if viewModel?.price == 0 {
                priceView.isHidden = true
            } else {
                priceView.isHidden = false
            }
        }
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureAll()
    }
    
    
    // MARK: - Configuration
    private func configureAll() {
        configureCell()
        configureSubviews()
    }
    
    private func configureSubviews() {
        priceView.layer.cornerRadius = 7
        priceView.layer.masksToBounds = true
    }
}
