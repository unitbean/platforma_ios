//
//  ChoosenEventsChoosenEventsTableViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

protocol ChoosenEventsTableViewDataSourceDelegate: class {

}

class ChoosenEventsCollectionViewDataSource: NSObject, UICollectionViewDataSource {

    var viewModels: [EventViewModel] = []

    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc:ChoosenEventsViewController!

    /// Используется для сообщения ViewController о взаимодействии с tableView
    weak var delegate: ChoosenEventsTableViewDataSourceDelegate?
    
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ChoosenEventCollectionViewCell.self), for: indexPath) as! ChoosenEventCollectionViewCell
        cell.viewModel = viewModels[indexPath.row]
        return cell
    }
}

//MARK: - UICollectionViewDelegate
extension ChoosenEventsCollectionViewDataSource: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        vc.routeToDetailedEvent(modelIndex: indexPath.row)
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension ChoosenEventsCollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: 245)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 1.0, bottom: 1.0, right: 1.0)
    }
}














