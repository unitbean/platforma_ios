//
//  ChoosenEventsChoosenEventsDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля ChoosenEvents
struct ChoosenEventsDataProvider: ChoosenEventsDataProviderInput {
    
    let dataStore: ChoosenEventsDataStore
    let apiService: ChoosenEventsAPIServiceProtocol

    init(dataStore: ChoosenEventsDataStore, apiService: ChoosenEventsAPIServiceProtocol) {
        self.dataStore = dataStore
        self.apiService = apiService
    }

    // Предоставление данных согласно определенным Use cases

    func getEvents(completion: @escaping ([EventModel]?, String?) -> Void) {
        apiService.getEvents { (events, error) in
            if let error = error {
                completion(nil, error)
            } else if let events = events {
                completion(events.events, nil)
            }
        }
    }
    
    func storeChoosenEvents(events: ChoosenEventsModel?) {
        self.dataStore.choosenEvents = events
    }
    
    
    func getStoredData<T>() -> [T]? where T : Equatable {
        return dataStore.choosenEvents?.events as? [T]
    }
}
