//
//  ChoosenEventsChoosenEventsAPIServiceProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

protocol ChoosenEventsAPIServiceProtocol: class {
    func getEvents(completion: @escaping (ChoosenEventsModel?, String?) -> Void)
}


extension ChoosenEventsAPIServiceProtocol where Self: APIService {
    func getEvents(completion: @escaping (ChoosenEventsModel?, String?) -> Void) {
        let urlString = "/event"
        convertResult(URLString: urlString, requestType: HTTPMethod.GET, params: nil, completion: completion)
    }
}



