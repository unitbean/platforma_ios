//
//  ChoosenEventsChoosenEventsViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit



class ChoosenEventsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyPlaceholderView: UIView!
    
    let collectionViewDataSource = ChoosenEventsCollectionViewDataSource()
    let configurator = ChoosenEventsConfigurator()
    var interactor: ChoosenEventsInteractorInput!
    var router: ChoosenEventsRouterProtocol!
    
    var isStatusBarHidden = false

    override var prefersStatusBarHidden: Bool {
        return isStatusBarHidden
    }
    
    var cellIdentifier = String(describing: ChoosenEventCollectionViewCell.self)

    var state = ChoosenEvents.ShowEvents.ViewControllerState.loading


    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configureModule(viewController: self)
        setupCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        //emptyPlaceholderView.isHidden = true
        setupNavigationBar()
        displayInitial()
    }
    
    private func displayInitial() {
        showProgress()
        interactor.loadChoosenEvents(request: ChoosenEvents.ShowEvents.Request())
    }
    
    private func setupCollectionView() {
        collectionViewDataSource.vc = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collectionView.alwaysBounceVertical = true
        collectionView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0)
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDataSource
    }
    
    private func setupNavigationBar() {
        self.parent?.title = "Мои события"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = true
    }
}

//MARK: - ChoosenEventsViewControllerInput
extension ChoosenEventsViewController: ChoosenEventsViewControllerInput {
    func routeToDetailedEvent(modelIndex: Int) {
        isStatusBarHidden = true
        router.routeToDetailedEvent(fromVc: self, modelIndex: modelIndex)
    }
    
    func displayChoosenEvents(viewModel: ChoosenEvents.ShowEvents.ViewModel) {
        state = viewModel.state
        switch state {
        case let .error(message):
            displayNoResult()
            hideProgress()
        case let .result(eventsViewModels):
            collectionViewDataSource.viewModels = eventsViewModels
            hideNoResult()
            updateCollectionViewData()
            hideProgress()
        case .emptyResult:
            displayNoResult()
            hideProgress()
        default: break
        }
    }
}

//MARK: - ChoosenEventsViewInput
extension ChoosenEventsViewController: ChoosenEventsViewInput {

    func showCollection() {
        collectionView.isHidden = false
    }

    func displayNoResult() {
        collectionView.isHidden = true
        emptyPlaceholderView.isHidden = false
    }
    
    func hideNoResult() {
        emptyPlaceholderView.isHidden = true
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        // TODO: - Отображение ошибки соединения
    }

    func hideConnectionError() {
        // TODO: - Скрыть отображение ошибки соединения
    }

    func showProgress() {
        
    }

    func hideProgress() {
        
    }

    func updateCollectionViewData() {
        self.showCollection()
        self.collectionView.reloadSections([0])
    }
}

extension ChoosenEventsViewController: DetailedEventViewControllerDelegate {
    
    func routeToSubscriptionPurchase() {
        router.routeToSubPurchase(fromVc: self)
    }
    
    func updateCards() {
        self.interactor.loadChoosenEvents(request: ChoosenEvents.ShowEvents.Request())
        self.isStatusBarHidden = false
    }
}














