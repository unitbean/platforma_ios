//
//  ChoosenEventsChoosenEventsDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum ChoosenEvents {
    // MARK: Use cases
    enum ShowEvents {
        struct Request {
        }

        struct Response {
            var result: ShowEvents.RequestResult
        }

        struct ViewModel {
            var state: ViewControllerState
        }
        
        enum ViewControllerState {
            case loading
            case result([EventViewModel])
            case emptyResult
            case error(message: String)
        }
        
        enum RequestResult {
            case failure(String)
            case success(ChoosenEventsModel)
        }
    }
}


