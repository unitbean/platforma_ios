//
//  ChoosenEventsChoosenEventsConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class ChoosenEventsConfigurator {

    func configureModule(viewController: ChoosenEventsViewController) {
        let apiService = DIAssembly.instance().api
        let dataStore = DIAssembly.instance().dataStoresList.choosenEventsDataStore
        let presenter = ChoosenEventsPresenter(viewController: viewController)
        let dataProvider = ChoosenEventsDataProvider(dataStore: dataStore, apiService: apiService)
        let interactor = ChoosenEventsInteractor(dataProvider: dataProvider, presenter: presenter)
        let router = ChoosenEventsRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
