//
//  ChoosenEventsChoosenEventsRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class ChoosenEventsRouter: ChoosenEventsRouterProtocol {

    var dataStoresList: DataStoresList

    func routeToDetailedEvent(fromVc: ChoosenEventsViewController, modelIndex: Int) {
        let fromVcDataStore = dataStoresList.choosenEventsDataStore
        guard let choosenEvents = fromVcDataStore.choosenEvents, let events = choosenEvents.events, !events.isEmpty else { return }
        dataStoresList.detailedEventDataStore.eventModel = events[modelIndex]
        let toVc = UIStoryboard(name: "DetailedEvent", bundle: nil).instantiateInitialViewController() as! DetailedEventViewController
        toVc.delegate = fromVc
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        toVc.modalPresentationCapturesStatusBarAppearance = true
        fromVc.present(toVc, animated: true) {
            toVc.showInterestedButton()
            toVc.showCloseButton()
        }
    }
    
    func routeToSubPurchase(fromVc: ChoosenEventsViewController) {
        let toVc = UIStoryboard(name: "SubPurchase", bundle: nil).instantiateInitialViewController() as! SubPurchaseViewController
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        fromVc.navigationController?.present(toVc, animated: true, completion: nil)
    }




    init(dataStoresList: DataStoresList) {
        self.dataStoresList = dataStoresList
    }
}
