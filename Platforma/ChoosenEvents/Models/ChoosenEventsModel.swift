//
//  ChoosenEventsChoosenEventsModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct ChoosenEventsModel: Decodable {

    var events: [EventModel]?

    private enum CodingKeys: String, CodingKey {
        case events = "result"
    }
}
