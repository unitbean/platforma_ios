//
//  ChoosenEventsChoosenEventsPresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class ChoosenEventsPresenter: ChoosenEventsPresenterInput {

    weak var viewController: ChoosenEventsViewControllerInput?
    private let timeFormatter = TimeFormatWorker()

    required init(viewController: ChoosenEventsViewControllerInput) {
        self.viewController = viewController
    }
    
    

    func presentChoosenEvents(response: ChoosenEvents.ShowEvents.Response) {
        var viewModel: ChoosenEvents.ShowEvents.ViewModel?

        switch response.result {
        case let .failure(error):
            viewModel = ChoosenEvents.ShowEvents.ViewModel(state: .error(message: error))
        case let .success(result):
            if let events = result.events {
                if events.isEmpty {
                    viewModel = ChoosenEvents.ShowEvents.ViewModel(state: .emptyResult)
                } else {
                    let mapped = result.events!.map {
                        EventViewModel(type               : $0.typeTitle,
                                       title            : $0.title,
                                       startDate        : timeFormatter.convertMillsToDateString($0.startDate, dateFormat: "dd.MM / HH:mm"),
                                       placeTitle       : $0.placeTitle,
                                       eventPic         : $0.mainPic,
                                       freeForPremium   : $0.freeForPremium,
                                       price            : $0.price,
                                       status           : $0.status,
                                       isAd             : $0.isAd
                        )
                    }
                    viewModel = ChoosenEvents.ShowEvents.ViewModel(state: .result(mapped))
                }
            }
        }
        guard let unwrappedViewModel = viewModel else { return }
        viewController?.displayChoosenEvents(viewModel: unwrappedViewModel)
    }
}
