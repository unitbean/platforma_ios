//
//  SubPurchasePriceNoStrikeTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 15.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class SubPurchasePriceNoStrikeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var monthsLabel: UILabel!
    @IBOutlet weak var popularView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    func configureContent() {
        priceLabel.text = "299 ₽"
        monthsLabel.text = "1 мес."
        popularView.isHidden = true
    }
    
    private func configureCell() {
        mainContainerView.layer.cornerRadius = 10
        popularView.layer.cornerRadius = 12
    }
    
}
