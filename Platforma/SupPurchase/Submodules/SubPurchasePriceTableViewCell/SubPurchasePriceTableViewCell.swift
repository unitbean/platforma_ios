//
//  SubPurchasePriceTableViewCellTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 12.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class SubPurchasePriceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var monthsLabel: UILabel!
    @IBOutlet weak var popularView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    func configureContent(rowIndex: Int) {
        switch rowIndex {
        case 2:
            priceLabel.text = "1490 ₽"
            oldPriceLabel.text = "Экономия 305 ₽"
            monthsLabel.text = "6 мес."
            popularView.isHidden = true
        case 3:
            priceLabel.text = "2390 ₽"
            oldPriceLabel.text = "Экономия 1200 ₽"
            monthsLabel.text = "12 мес."
            popularView.isHidden = true
        default: break
        }
    }
    
    private func configureCell() {
        mainContainerView.layer.cornerRadius = 10
        popularView.layer.cornerRadius = 12
    }
}
