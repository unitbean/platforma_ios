//
//  SubPurchaseViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 12.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

protocol SubPurchaseViewControllerDelegate: class {
    func reloadData()
}


class SubPurchaseViewController: UIViewController {
    
    @IBOutlet weak var tableView: DetailedEventTableView!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var headerImageHeightConstraint: NSLayoutConstraint!
    
    let cellIdentifiers = [String(describing: SubPurchasePriceNoStrikeTableViewCell.self),
                            String(describing: SubPurchaseAdvantagesTableViewCell.self),
                            String(describing: SubPurchasePriceTableViewCell.self)]
    
    let activityIndicator = ActivityIndicator()
    
    weak var delegate: SubPurchaseViewControllerDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        addGradient()
        activityIndicator.setup(with: self.view, backgroundColor: UIColor.black.withAlphaComponent(0.5), indicatorColor: UIColor.white)
        IAPService.shared.getProducts()
        IAPService.shared.delegate = self
    }

    
    @IBAction func closeButtonDidTouch(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        delegate?.reloadData()
    }
    //MARK: - Private methods
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        for identifier in cellIdentifiers {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
        tableView.tableHeaderView?.frame.size.height = UIScreen.main.bounds.height / 3
        headerImageHeightConstraint.constant = UIScreen.main.bounds.height / 7
    }
    
    private func addGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [ColorConstants.adOrangeGradientColor.cgColor,
                           ColorConstants.adVioletGradientColor.cgColor,
                           ColorConstants.adBlueGradientColor.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        view.layer.insertSublayer(gradient, at: 0)
    }
}

//MARK: - UITableViewDataSource
extension SubPurchaseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SubPurchaseAdvantagesTableViewCell.self), for: indexPath) as! SubPurchaseAdvantagesTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SubPurchasePriceNoStrikeTableViewCell.self), for: indexPath) as! SubPurchasePriceNoStrikeTableViewCell
            cell.configureContent()
            return cell
        case 2, 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SubPurchasePriceTableViewCell.self), for: indexPath) as! SubPurchasePriceTableViewCell
            cell.configureContent(rowIndex: indexPath.row)
            return cell
        default: return UITableViewCell()
        }
    }
}

//MARK: - UITableViewDelegate
extension SubPurchaseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return UITableViewAutomaticDimension
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        activityIndicator.show()
        switch indexPath.row {
        case 1: IAPService.shared.purchaseSubscription(productId: IAPProduct.oneMonthSub.rawValue, sharedSecret: IAPService.shared.sharedSecret)
        case 2: IAPService.shared.purchaseSubscription(productId: IAPProduct.sixMonthSub.rawValue, sharedSecret: IAPService.shared.sharedSecret)
        case 3: IAPService.shared.purchaseSubscription(productId: IAPProduct.twelveMonthSub.rawValue, sharedSecret: IAPService.shared.sharedSecret)
        default: break
        }
    }
}

//MARK: - IAPServiceDelegate
extension SubPurchaseViewController: IAPServiceDelegate {
    func iAPServiceFinishedTransaction() {
        activityIndicator.hide()
    }
    
    func iAPServiceFinishedTransactionWithError() {
        activityIndicator.hide()
    }
    
    
}
