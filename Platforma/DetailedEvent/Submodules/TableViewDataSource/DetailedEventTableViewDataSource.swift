//
//  DetailedEventDetailedEventTableViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit



class DetailedEventTableViewDataSource: NSObject, UITableViewDataSource {
    
    var viewModel: DetailedEventViewModel?
    let generator = UIImpactFeedbackGenerator(style: .heavy)
    private var makeTapticFeedback = true
    private var canBeDismissed = true
    fileprivate var possibleOffset: CGFloat {
        //MARK: - REFACTOR
        if UIDevice.current.deviceModel == .iPhone6 || UIDevice.current.deviceModel == .iPhone6S || UIDevice.current.deviceModel == .iPhone7 || UIDevice.current.deviceModel == .iPhone8 || UIDevice.current.deviceModel == .iPhone5S || UIDevice.current.deviceModel == .iPhoneSE {
            return -120
        } else {
            return -160
        }
    }

    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc: DetailedEventViewController!
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel?.price == 0 {
            return 4
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedEventTitleTableViewCell", for: indexPath) as! DetailedEventTitleTableViewCell
            cell.viewModel = viewModel
            return cell
        case 1, 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedEventDetailsTableViewCell", for: indexPath) as! DetailedEventDetailsTableViewCell
            if let viewModel = viewModel {
                cell.customize(indexPath: indexPath, viewModel: viewModel)
            }
            return cell
        case 3:
            if viewModel?.price == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedEventDescriptionTableViewCell", for: indexPath) as! DetailedEventDescriptionTableViewCell
                cell.viewModel = viewModel
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedEventDetailsPriceTableViewCell", for: indexPath) as! DetailedEventDetailsPriceTableViewCell
                cell.delegate = vc
                cell.viewModel = viewModel
                return cell
            }
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedEventDescriptionTableViewCell", for: indexPath) as! DetailedEventDescriptionTableViewCell
            cell.viewModel = viewModel
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return 154.5
        case 1, 2, 3, 4: return UITableViewAutomaticDimension
        default: return CGFloat.leastNonzeroMagnitude
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 200 && !vc.closeButtonIsHidden {
            vc.hideCloseButton()
        } else if scrollView.contentOffset.y < 200 && vc.closeButtonIsHidden {
            vc.showCloseButton()
        } else if scrollView.contentOffset.y <= possibleOffset {
            
            switch scrollView.panGestureRecognizer.state {
            case .began:
                makeTapticFeedback = true
            case .changed:
                if makeTapticFeedback {
                    makeTapticFeedback = false
                    generator.prepare()
                    generator.impactOccurred()
                }
            case .possible:
                if makeTapticFeedback {
                    makeTapticFeedback = false
                    generator.prepare()
                    generator.impactOccurred()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    if self.canBeDismissed == true {
                        self.canBeDismissed = false
                        self.vc.presentingViewController?.dismiss(animated: true, completion: nil)
                        self.vc.delegate?.updateCards()
                    }
                }
            default: break
            }
        }
    }
}

//MARK: - UITableViewDelegate
extension DetailedEventTableViewDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: IndexPath(row: 3, section: 0)) is DetailedEventDetailsPriceTableViewCell {
            if PlatformaUserDefaults.getUserPremiumStatus() != true {
                DispatchQueue.main.async {
                    self.vc.presentingViewController?.dismiss(animated: true, completion: {
                        self.vc.delegate?.routeToSubscriptionPurchase()
                    })
                }
            }
        }
    }
}














