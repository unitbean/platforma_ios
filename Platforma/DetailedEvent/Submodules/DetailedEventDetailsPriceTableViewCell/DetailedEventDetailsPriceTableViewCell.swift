//
//  DetailedEventDetailsPriceTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 05.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

protocol DetailedEventDetailsPriceTableViewCellDelegate: class {
    func premiumButtonDidTouch()
}

class DetailedEventDetailsPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var premiumButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceIcon: UIImageView!
    
    weak var delegate: DetailedEventDetailsPriceTableViewCellDelegate?
    
    var viewModel: DetailedEventViewModel? {
        didSet {
            priceIcon.image = #imageLiteral(resourceName: "icPrice").withRenderingMode(.alwaysOriginal)
            if let viewModel = viewModel, let price = viewModel.price {
                priceLabel.text = String(describing: price) + " ₽"
            }
            if viewModel?.freeForPremium == false {
                premiumButton.isHidden = true
            } else {
                premiumButton.isHidden = false
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    private func configureCell() {
        premiumButton.layer.cornerRadius = 12
    }
    
    @IBAction func premiumButtonDidTouch(_ sender: Any) {
        delegate?.premiumButtonDidTouch()
    }
}
