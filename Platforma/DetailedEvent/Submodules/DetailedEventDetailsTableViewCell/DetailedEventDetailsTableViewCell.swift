//
//  DetailedEventDetailsTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 05.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedEventDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailIcon: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func customize(indexPath: IndexPath, viewModel: DetailedEventViewModel) {
        switch indexPath.row {
        case 1:
            detailIcon.image = #imageLiteral(resourceName: "roundAccessTime24Px").withRenderingMode(.alwaysOriginal)
            detailLabel.text = viewModel.startDate
        case 2:
            detailIcon.image = #imageLiteral(resourceName: "roundPlace24Px").withRenderingMode(.alwaysOriginal)
            detailLabel.text = viewModel.placeTitle
        default: break
        }
    }
}
