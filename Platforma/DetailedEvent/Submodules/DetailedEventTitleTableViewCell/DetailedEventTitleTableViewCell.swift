//
//  DetailedEventTitleTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 04.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedEventTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    var viewModel: DetailedEventViewModel? {
        didSet {
            eventTypeLabel.text = viewModel?.type
            eventTitleLabel.text = viewModel?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
