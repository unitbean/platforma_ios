//
//  DetailedEventDescriptionTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 05.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedEventDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    var viewModel: DetailedEventViewModel? {
        didSet {
            descriptionLabel.text = viewModel?.description
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
