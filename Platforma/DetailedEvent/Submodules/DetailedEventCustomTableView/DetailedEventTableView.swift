//
//  DetailedEventTableView.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 04.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedEventTableView: UITableView {

    var headerHeight: NSLayoutConstraint?
    var headerBottom: NSLayoutConstraint?
    var headerGradiengHeight: NSLayoutConstraint?

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let header = tableHeaderView else { return }
        if let imageView = header.subviews.first as? UIImageView {
            headerHeight = imageView.constraints.filter{ $0.identifier == "headerHeight"}.first
            headerBottom = header.constraints.filter{ $0.identifier == "headerBottom"}.first
            
            imageView.layer.masksToBounds = true
        }
        
        let gradientView = header.subviews.filter{ $0 is DetailedPromoTableHeaderGradientView }.first
        
        if let gradient = gradientView {
            headerGradiengHeight = gradient.constraints.filter{ $0.identifier == "headerGradientHeight" }.first
        }
        
        let offsetY = -contentOffset.y
        headerBottom?.constant = offsetY >= 0 ? 0 : offsetY / 2
        headerHeight?.constant = max(header.bounds.height, header.bounds.height + offsetY)
        headerGradiengHeight?.constant = max(header.bounds.height, header.bounds.height + offsetY)
        
        header.clipsToBounds = offsetY <= 0
    }
}
