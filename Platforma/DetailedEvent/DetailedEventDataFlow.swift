//
//  DetailedEventDetailedEventDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum DetailedEvent {
    // MARK: Use cases
    enum Event {
        struct Request {
        }

        struct Response {
            var result: DetailedEvent.RequestResult
            var eventChoosen: Bool
        }

        struct ViewModel {
            var state: ViewControllerState
        }
    }
    
    enum SaveEvent {
        struct Request {
            
        }
        
        struct Response {
            var result: EventModel
            var eventChoosen: Bool
        }
        
        struct ViewModel {
            var state: ViewControllerState
        }
    }

    enum ViewControllerState {
        case loading
        case result(DetailedEventViewModel)
        case emptyResult
        case error(message: String)
    }

    enum RequestResult {
        case failure(String)
        case success(EventModel)
    }
}


