//
//  DetailedEventDetailedEventPresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class DetailedEventPresenter: DetailedEventPresenterInput {

    weak var viewController: DetailedEventViewControllerInput!

    required init(viewController: DetailedEventViewController) {
        self.viewController = viewController
    }

    
    
    //MARK: - Interface
    func presentEvent(response: DetailedEvent.Event.Response) {
        var viewModel: DetailedEvent.Event.ViewModel

        switch response.result {
        case let .failure(error):
            viewModel = DetailedEvent.Event.ViewModel(state: .error(message: error))
        case let .success(result):
            let eventViewModel = createViewModel(model: result, eventChoosen: response.eventChoosen)
            viewModel = DetailedEvent.Event.ViewModel(state: .result(eventViewModel))
        }
        viewController.displayEvent(viewModel: viewModel)
    }
    
    func presentChoosenEvent(response: DetailedEvent.SaveEvent.Response) {
        var viewModel: DetailedEvent.Event.ViewModel
        let result = response.result
        
        let eventViewModel = createViewModel(model: result, eventChoosen: response.eventChoosen)
        viewModel = DetailedEvent.Event.ViewModel(state: .result(eventViewModel))
        viewController.displayButtonTapped(viewModel: viewModel)
    }
    
    
    //MARK: - Private Methods
    private func createViewModel(model: EventModel, eventChoosen: Bool) -> DetailedEventViewModel {
        let timeFormatter = TimeFormatWorker()
        let showInterestestedButton: Bool?
        if let linkToRegister = model.linkToRegister, linkToRegister != "" {
            showInterestestedButton = true
        } else {
            showInterestestedButton = false
        }
        let eventViewModel = DetailedEventViewModel(type               : model.typeTitle,
                                                    title            : model.title,
                                                    startDate        : timeFormatter.convertMillsToDateString(model.startDate, dateFormat: "dd MMMM HH:mm"),
                                                    placeTitle       : model.placeTitle,
                                                    eventPic         : model.mainPic,
                                                    freeForPremium   : model.freeForPremium,
                                                    price            : model.price,
                                                    status           : model.status,
                                                    description      : model.description,
                                                    showRegisterButton  : showInterestestedButton,
                                                    buttonStateSelected : eventChoosen
        )
        return eventViewModel
    }
}
