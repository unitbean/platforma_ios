//
//  DetailedEventDetailedEventViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit
import SafariServices


protocol DetailedEventViewControllerDelegate: class {
    func updateCards()
    func routeToSubscriptionPurchase()
}


class DetailedEventViewController: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet var tableView: DetailedEventTableView!
    @IBOutlet weak var tableViewHeaderImageView: UIImageView!
    @IBOutlet var headerImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var interestedButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var interestedButtonBottomConstraint: NSLayoutConstraint!
    
    let tableViewDataSource = DetailedEventTableViewDataSource()
    let cellIdentifiers = [ String(describing: DetailedEventTitleTableViewCell.self),
                            String(describing: DetailedEventDetailsTableViewCell.self),
                            String(describing: DetailedEventDetailsPriceTableViewCell.self),
                            String(describing: DetailedEventDescriptionTableViewCell.self)]

    let configurator = DetailedEventConfigurator()

    var interactor: DetailedEventInteractorInput!
    var router: DetailedEventRouterProtocol!
    
    weak var delegate: DetailedEventViewControllerDelegate?

    var state = DetailedEvent.ViewControllerState.loading

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var closeButtonIsHidden: Bool = false
    

    
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configureModule(viewController: self)
        setupSubViews()
        displayInitial()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @IBAction func interestedButtonDidTouch(_ sender: Any) {
        interactor.saveDeleteEvent(request: DetailedEvent.SaveEvent.Request(), completion: { linkToRegister in
            if let url = URL(string: linkToRegister) {
                let svc = SFSafariViewController(url: url)
                self.present(svc, animated: true, completion: nil)
            }
        })
    }
    @IBAction func closeButtonDidTouch(_ sender: Any) {
        DispatchQueue.main.async {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            self.delegate?.updateCards()
        }
    }
    
    //MARK: - Private methods
    private func setupTableView() {
        for identifier in cellIdentifiers {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDataSource
        tableViewDataSource.vc = self
        tableView.tableHeaderView?.frame.size.height = UIScreen.main.bounds.height / 2
        headerImageViewHeight.constant = UIScreen.main.bounds.height / 2
        DispatchQueue.main.async {
            [weak self] in
            self?.tableView.layoutIfNeeded()
        }
    }
    
    private func setupSubViews() {
        setupTableView()
        
        interestedButton.layer.cornerRadius = 10
        interestedButton.layer.masksToBounds = false
        interestedButton.layer.shadowColor = UIColor.black.cgColor
        interestedButton.layer.shadowOpacity = 0.3
        interestedButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        interestedButton.layer.shadowRadius = 8
        
        closeButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        closeButton.alpha = 0
    }
    
    private func displayInitial() {
        showProgress()
        interactor.loadEvent(request: DetailedEvent.Event.Request())
    }
    
    fileprivate func changeButtonColor(eventChoosen: Bool?) {
        if eventChoosen == false {
            self.interestedButton.backgroundColor = UIColor(red: 7.0/255.0, green: 253.0/255.0, blue: 238.0/255.0, alpha: 1.0)
            self.interestedButton.setTitleColor(UIColor(red: 2.0/255.0, green: 10.0/255.0, blue: 20.0/255.0, alpha: 1.0), for: .normal)
            self.interestedButton.setTitle("МНЕ ИНТЕРЕСНО", for: .normal)
        } else {
            self.interestedButton.backgroundColor = UIColor.white
            self.interestedButton.setTitleColor(UIColor(red: 203.0/255.0, green: 203.0/255.0, blue: 203.0/255.0, alpha: 1.0), for: .normal)
            self.interestedButton.setTitle("БОЛЬШЕ НЕ ИНТЕРЕСНО", for: .normal)
        }
    }
}

//MARK: - DetailedEventViewControllerInput
extension DetailedEventViewController: DetailedEventViewControllerInput {
    func displayButtonTapped(viewModel: DetailedEvent.Event.ViewModel) {
        state = viewModel.state
        switch state {
        case let .result(item):
            UIView.animate(withDuration: 0.3) {
               self.changeButtonColor(eventChoosen: item.buttonStateSelected)
            }
        default: break
        }
    }
    
    func displayEvent(viewModel: DetailedEvent.Event.ViewModel) {
        state = viewModel.state
        switch state {
        case let .error(message):
            print("error \(message)")
        case let .result(item):
            hideProgress()
            if item.showRegisterButton == true {
                interestedButton.isHidden = false
            } else {
                interestedButton.isHidden = true
            }
            changeButtonColor(eventChoosen: item.buttonStateSelected)
            tableViewDataSource.viewModel = item
            if let eventPic = item.eventPic {
                tableViewHeaderImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + eventPic))
            }
            updateTableViewData()
        case .emptyResult:
            print("empty result")
        default: break
        }
    }
}

//MARK: - DetailedEventViewInput
extension DetailedEventViewController: DetailedEventViewInput {

    func showTable() {
        // TODO: - Отображение tableView
    }
    
    func showCloseButton() {
        closeButtonIsHidden = false
        closeButton.isHidden = closeButtonIsHidden
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .curveEaseInOut, animations: { [weak self] in
            self?.closeButton.alpha = 1
            self?.closeButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self?.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.4, delay: 0.15, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: UIViewAnimationOptions.curveEaseIn, animations: { [weak self] in
            self?.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 4)
        }, completion: nil)
    }
    
    func hideCloseButton() {
        closeButtonIsHidden = true
        UIView.animate(withDuration: 0.2, animations: {
            [weak self] in
            self?.closeButton.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            self?.closeButton.alpha = 0
        }) {_ in
            self.closeButton.isHidden = self.closeButtonIsHidden
        }
    }
    
    func showInterestedButton() {
        interestedButtonBottomConstraint.constant = 20
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func displayNoResult() {
        // TODO: - Отображение отсутствия результата
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        // TODO: - Отображение ошибки соединения
    }

    func hideConnectionError() {
        // TODO: - Скрыть отображение ошибки соединения
    }

    func showProgress() {
        // TODO: - Показать прогресс-бар
    }

    func hideProgress() {
        // TODO: - Скрыть прогресс-бар
    }

    func updateTableViewData() {
        showTable()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

//MARK: - CardPresentingAnimatorDelegate
extension DetailedEventViewController: CardPresentingAnimatorDelegate {
    func animatingDidEnd() {
        for cellIndex in 1...tableView.numberOfRows(inSection: 0) - 1 {
            self.tableView.cellForRow(at: IndexPath(item: cellIndex, section: 0))?.transform = CGAffineTransform(translationX: 0, y: -400)
            self.tableView.cellForRow(at: IndexPath(item: cellIndex, section: 0))?.contentView.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .curveEaseInOut, animations: {
                self.tableView.cellForRow(at: IndexPath(item: cellIndex, section: 0))?.transform = CGAffineTransform(translationX: 0, y: 0)
                self.tableView.cellForRow(at: IndexPath(item: cellIndex, section: 0))?.contentView.alpha = 1
            }, completion: nil)
        }
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .curveEaseInOut, animations: {
            self.closeButton.alpha = 1
            self.closeButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.4, delay: 0.15, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 4)
        }, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showInterestedButton()
        }
    }
}

//MARK: - DetailedEventDetailsPriceTableViewCellDelegate
extension DetailedEventViewController: DetailedEventDetailsPriceTableViewCellDelegate {
    func premiumButtonDidTouch() {
        if PlatformaUserDefaults.getUserPremiumStatus() != true {
            DispatchQueue.main.async {
                self.presentingViewController?.dismiss(animated: true, completion: {
                    self.delegate?.routeToSubscriptionPurchase()
                })
            }
        }
    }
}















