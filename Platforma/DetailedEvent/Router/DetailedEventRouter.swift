//
//  DetailedEventDetailedEventRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class DetailedEventRouter: DetailedEventRouterProtocol {

    var dataStoresList: /*App name*/DataStoresList

    func routeToSubPurchase(fromVc: UIViewController) {
        let toVc = UIStoryboard(name: "SubPurchase", bundle: nil).instantiateInitialViewController() as! SubPurchaseViewController
        toVc.modalPresentationStyle = .overFullScreen
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        fromVc.navigationController?.present(toVc, animated: true, completion: nil)
    }





    init(dataStoresList: /*App name*/DataStoresList) {
        self.dataStoresList = dataStoresList
    }
}
