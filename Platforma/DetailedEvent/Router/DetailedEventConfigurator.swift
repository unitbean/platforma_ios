//
//  DetailedEventDetailedEventConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class DetailedEventConfigurator {

    func configureModule(viewController: DetailedEventViewController) {
        let apiService = DIAssembly.instance().api
        let dataStore = DIAssembly.instance().dataStoresList.detailedEventDataStore
        let presenter = DetailedEventPresenter(viewController: viewController)
        let dataProvider = DetailedEventDataProvider(dataStore: dataStore, apiService: apiService)
        let interactor = DetailedEventInteractor(dataProvider: dataProvider, presenter: presenter)
        let router = DetailedEventRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
