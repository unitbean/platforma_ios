//
//  DetailedEventDetailedEventProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit

/// DetailedEvent DataProvider Interface
protocol DetailedEventDataProviderInput {
    func getEvent(completion: @escaping (EventModel?, String?) -> Void)
    func saveEvent(completion: @escaping (EventModel) -> Void)
}

/// DetailedEvent Router Interface
protocol DetailedEventRouterProtocol {
    func routeToSubPurchase(fromVc: UIViewController)
}

/// DetailedEvent ViewController Interface
protocol DetailedEventViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayEvent(viewModel: DetailedEvent.Event.ViewModel)
    func displayButtonTapped(viewModel: DetailedEvent.Event.ViewModel)
}

/// DetailedEvent View Interface
protocol DetailedEventViewInput: class {
    func displayNoResult()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateTableViewData()
}

/// DetailedEvent Interactor Interface
protocol DetailedEventInteractorInput {
    func loadEvent(request: DetailedEvent.Event.Request)
    func saveDeleteEvent(request: DetailedEvent.SaveEvent.Request, completion: @escaping (String) -> Void)
}

/// DetailedEvent Presenter Interface
protocol DetailedEventPresenterInput: class {
    func presentEvent(response: DetailedEvent.Event.Response)
    func presentChoosenEvent(response: DetailedEvent.SaveEvent.Response)
}






