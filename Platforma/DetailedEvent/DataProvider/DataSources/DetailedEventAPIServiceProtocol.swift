//
//  DetailedEventDetailedEventAPIServiceProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

protocol DetailedEventAPIServiceProtocol: class {
    func getEvent(completion: @escaping (EventModel?, String?) -> Void)
}


extension DetailedEventAPIServiceProtocol where Self: APIService {
    func getEvent(completion: @escaping (EventModel?, String?) -> Void) {
        // Реализация обращения к APIService
        // ...
    }
}



