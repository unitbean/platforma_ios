//
//  DetailedEventDetailedEventDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля DetailedEvent
struct DetailedEventDataProvider: DetailedEventDataProviderInput {
    let dataStore: DetailedEventDataStore
    let apiService: DetailedEventAPIServiceProtocol

    init(dataStore: DetailedEventDataStore, apiService: DetailedEventAPIServiceProtocol) {
        self.dataStore = dataStore
        self.apiService = apiService
    }

    // Предоставление данных согласно определенным Use cases

    func getEvent(completion: @escaping (EventModel?, String?) -> Void) {
        if let model = dataStore.eventModel {
            return completion(model, nil)
        }
    }
    
    func saveEvent(completion: @escaping (EventModel) -> Void) {
        if let model = dataStore.eventModel, let eventId = model.id {
            PlatformaUserDefaults.setChoosenEventId(id: eventId)
            return completion(model)
        }
    }
}
