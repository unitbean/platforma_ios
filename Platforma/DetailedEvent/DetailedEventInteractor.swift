//
//  DetailedEventDetailedEventInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// Класс для описания бизнес-логики модуля DetailedEvent
class DetailedEventInteractor: DetailedEventInteractorInput {

    let presenter: DetailedEventPresenterInput
    let dataProvider: DetailedEventDataProviderInput

    required init(dataProvider: DetailedEventDataProvider, presenter: DetailedEventPresenter) {
        self.dataProvider = dataProvider
        self.presenter = presenter
    }

    func loadEvent(request: DetailedEvent.Event.Request) {
        dataProvider.getEvent { [unowned self] (model, error) in
            let result: DetailedEvent.RequestResult
            if let model = model {
                result = .success(model)
            } else if let error = error {
                result = .failure(error)
            } else {
                result = .failure("Ошибка получения данных")
            }
            
            if let savedEvents = PlatformaUserDefaults.getChoosenEventsIds(), let id = model?.id, savedEvents.contains(id) {
               self.presenter.presentEvent(response: DetailedEvent.Event.Response(result: result, eventChoosen: true))
            } else {
                self.presenter.presentEvent(response: DetailedEvent.Event.Response(result: result, eventChoosen: false))
            }
        }
    }
    
    func saveDeleteEvent(request: DetailedEvent.SaveEvent.Request, completion: @escaping (String) -> Void) {
        dataProvider.getEvent { [unowned self] (model, error) in
            if let model = model, let id = model.id {
                guard let savedEvents = PlatformaUserDefaults.getChoosenEventsIds() else {
                    PlatformaUserDefaults.setChoosenEventId(id: id)
                    self.presenter.presentChoosenEvent(response: DetailedEvent.SaveEvent.Response(result: model, eventChoosen: true))
                    if let linkToRegister = model.linkToRegister {
                        completion(linkToRegister)
                    }
                    return
                }
                if !savedEvents.contains(id) {
                    PlatformaUserDefaults.setChoosenEventId(id: id)
                    self.presenter.presentChoosenEvent(response: DetailedEvent.SaveEvent.Response(result: model, eventChoosen: true))
                    if let linkToRegister = model.linkToRegister {
                        completion(linkToRegister)
                    }
                } else {
                    PlatformaUserDefaults.removeChoosenEventId(id: id)
                    self.presenter.presentChoosenEvent(response: DetailedEvent.SaveEvent.Response(result: model, eventChoosen: false))
                }
            }
        }
    }
}
