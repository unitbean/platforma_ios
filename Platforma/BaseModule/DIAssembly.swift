//
//  DIAssembly.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import EasyDi


class DIAssembly: Assembly {
    
    var api: APIService {
        return define(scope: .lazySingleton, init: APIService()) {
            return $0
        }
    }
    
    var dataStoresList: DataStoresList {
        return define(scope: .lazySingleton, init: DataStoresList()) {
            return $0
        }
    }
    
    var socialNetworkService: SocialNetworkService {
        return define(scope: .lazySingleton, init: SocialNetworkService()) {
            return $0
        }
    }
}
