//
//  TimeFormatWorker.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 05.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


class TimeFormatWorker: NSObject {
    
    func convertMillsToDateString(_ mills: Double?, dateFormat: String) -> String {
        guard let millis = mills else { return "" }
        let timeInterval = millis
        let date = Date(timeIntervalSince1970: Double(timeInterval) / 1000)
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = dateFormat
        
        let formattedString = formatter.string(from: date)
        return formattedString
    }
}
