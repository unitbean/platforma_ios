//
//  AlertWorker.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 13.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit


enum PreferredAlertAction {
    case left
    case right
}

typealias AlertActionHandler = ((UIAlertAction) -> Void)?

class AlertWorker: NSObject {
    
    static func showAlertWithOneButton(title: String?, message: String?, handler: AlertActionHandler = nil, fromVC: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        
        DispatchQueue.main.async {
            fromVC.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showAlert(title: String?,
                          message: String?,
                          preferredText: String? = "Да",
                          text: String? = "Нет",
                          handler: AlertActionHandler = nil,
                          handler2: AlertActionHandler = nil,
                          preferredAction: PreferredAlertAction = .right,
                          fromVC: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let button1 = UIAlertAction(title: preferredText, style: .default, handler: handler)
        let button2 = UIAlertAction(title: text, style: .default, handler: handler2)
        
        switch preferredAction {
        case .left:
            alertController.addAction(button1)
            alertController.addAction(button2)
        case .right:
            alertController.addAction(button2)
            alertController.addAction(button1)
        }
        
        alertController.preferredAction = button1
        DispatchQueue.main.async {
            fromVC.present(alertController, animated: true, completion: nil)
        }
    }
}
