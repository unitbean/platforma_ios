//
//  ActivityIndicator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 14.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class ActivityIndicator {
    
    var activityIndicator: NVActivityIndicatorView
    var blockView: UIView
    var view: UIView
    
    init() {
        activityIndicator = NVActivityIndicatorView(frame: .zero)
        blockView = UIView()
        view = UIView()
    }
    
    public func setup(with view: UIView, backgroundColor: UIColor = UIColor.white, indicatorColor: UIColor = UIColor.black, type: NVActivityIndicatorType = NVActivityIndicatorType.lineScaleParty, frame: CGRect = CGRect(x: 0, y: 0, width: 50, height: 50)) {
        self.view = view
        blockView.frame = view.bounds
        blockView.isUserInteractionEnabled = true
        blockView.backgroundColor = backgroundColor
        blockView.alpha = 0.0
        
        activityIndicator = NVActivityIndicatorView(frame: .zero, type: type, color: indicatorColor, padding: nil)
        activityIndicator.bounds = frame
        activityIndicator.alpha = 0.0
    }
    
    public func show() {
        DispatchQueue.main.async {
            self.view.addSubview(self.blockView)
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            self.activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            self.activityIndicator.startAnimating()
            
            UIView.animate(withDuration: 0.3) {
                self.blockView.alpha = 1.0
                self.activityIndicator.alpha = 1.0
            }
        }
    }
    
    public func hide() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.blockView.alpha = 0.0
                self.activityIndicator.alpha = 0.0
            }) { (finished) in
                if finished {
                    self.blockView.removeFromSuperview()
                    self.activityIndicator.removeFromSuperview()
                }
            }
        }
    }
}
