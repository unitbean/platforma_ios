//
//  SocialUserServiceModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


enum SocialType:String {
    case FACEBOOK = "fb"
    case VK = "vk"
}

class SocialUserServiceModel: NSObject {
    var socialType:SocialType?
    var socialID:String?
    var info:[String: AnyObject]?
    var token:String?
    var expires_in:Int?
    var avatar:String?
    var name: String?
    var lastName: String?
    var email:String?
    var phone:String?
}
