//
//  TokenModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


struct TokenModel {
    var socialType: SocialType?
    var token: String?
}
