//
//  UIImageViewExtension.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit



extension UIView {
    func applyGradientToView(colors: [UIColor]) {
        let gradientView = UIView(frame: self.frame)
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = gradientView.frame
        gradient.colors = colors.map { $0.cgColor }
        gradientView.layer.insertSublayer(gradient, at: 0)
        
        self.addSubview(gradientView)
        self.bringSubview(toFront: gradientView)
    }
}
