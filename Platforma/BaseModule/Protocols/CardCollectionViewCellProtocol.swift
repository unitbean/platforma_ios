//
//  CardCollectionViewCellProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 08.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit


protocol CardCollectionViewCellProtocol: class {
    func configureCell()
}

extension CardCollectionViewCellProtocol where Self: UICollectionViewCell{
    func configureCell() {
        self.contentView.backgroundColor = .white
        self.contentView.layer.cornerRadius = 14
        self.contentView.layer.masksToBounds = true
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 12.0/255.0, alpha: 0.12).cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0, height: 22)
        self.layer.shadowRadius = 22
    }
}
