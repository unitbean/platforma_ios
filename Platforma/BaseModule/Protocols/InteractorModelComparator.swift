//
//  InteractorModelComparator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 07.01.2019.
//  Copyright © 2019 UnitBean. All rights reserved.
//

import Foundation

protocol InteractorModelComparator {
    func isEqualToStoredModels<T: Equatable>(models: [T]?) -> Bool
}


