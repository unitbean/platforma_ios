//
//  VKUserDataAPIServiceProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09.01.2019.
//  Copyright © 2019 UnitBean. All rights reserved.
//

import Foundation
import VK_ios_sdk


protocol VKUserDataAPIServiceProtocol: class {
    func getVKUserModels(token: String, completion: @escaping (SocialUserServiceModel?) -> Void)
}

extension APIService: VKUserDataAPIServiceProtocol {
    func getVKUserModels(token: String, completion: @escaping (SocialUserServiceModel?) -> Void) {
        let fields = "first_name, last_name, photo_max"
        let request: VKRequest = VKRequest(method: "users.get", parameters: ["fields": fields, "access_token": token])
        request.execute(resultBlock: { (response) in
            debugPrint(response?.json)
            let json = response?.json as? Array<Dictionary<String, Any>>
            
            if let json = json?.first {
                let socialUser = SocialUserServiceModel()
                socialUser.socialType = SocialType.VK
                socialUser.socialID = json["id"] as? String
                socialUser.avatar = json["photo_max"] as? String
                socialUser.name = json["first_name"] as? String
                socialUser.lastName = json["last_name"] as? String
                
                completion(socialUser)
            } else {
                completion(nil)
            }
        }) { (error) in
            debugPrint(error?.localizedDescription)
            completion(nil)
        }
    }
}
