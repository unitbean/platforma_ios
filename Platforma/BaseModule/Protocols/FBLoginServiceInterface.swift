//
//  FBLoginServiceInterface.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 10.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


protocol FBLoginServiceInterface: class {
    func loginFB()
    func logOutFB()
}
