//
//  FBUserDataAPIServiceProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09.01.2019.
//  Copyright © 2019 UnitBean. All rights reserved.
//

import Foundation
import FBSDKLoginKit

protocol FBUserDataAPIServiceProtocol: class {
    func getFacebookPersonalInformation(completion: @escaping (SocialUserServiceModel?) -> Void)
}

extension APIService: FBUserDataAPIServiceProtocol {
    func getFacebookPersonalInformation(completion: @escaping (SocialUserServiceModel?) -> Void) {
        let fields = "id, name, link, first_name, last_name, picture.type(large), email, friends, friendlists"
        
        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields" : fields]).start() { (connection, result, error) in
            if error == nil && result != nil {
                PlatformaUserDefaults.setSocialToken(["fb" : FBSDKAccessToken.current().tokenString])
                
                let socialUser = SocialUserServiceModel()
                socialUser.socialType = SocialType.FACEBOOK
                socialUser.socialID = (result as AnyObject)["id"] as? String
                socialUser.info = result as? [String:AnyObject]
                socialUser.token = FBSDKAccessToken.current().tokenString
                
                if (result as AnyObject)["picture"] != nil {
                    socialUser.avatar = (result as AnyObject)["picture"] as? String
                } else {
                    if (result as AnyObject)["data"] != nil {
                        socialUser.avatar = (result as AnyObject)["data"] as? String
                    } else {
                        socialUser.avatar = (result as AnyObject)["url"] as? String
                    }
                }
                if let firstName = (result as AnyObject)["first_name"] as? String {
                    socialUser.name = firstName
                }
                if let lastName = (result as AnyObject)["last_name"] as? String {
                    socialUser.lastName = lastName
                }
                completion(socialUser)
            } else {
                completion(nil)
            }
        }
    }
}
