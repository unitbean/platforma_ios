//
//  LoginService.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 10.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import VK_ios_sdk
import FBSDKCoreKit
import FBSDKLoginKit


protocol LoginServiceDelegate: class {
    func vkSdkAccessAuthorizationFinished()
    func fbSdkAccessAuthorizationFinished()
}


class SocialNetworkService: NSObject {
    
    private lazy var fbLoginManager = FBSDKLoginManager()
    
    fileprivate let vkScope = ["id", "first_name", "last_name", "photo_max"]
    fileprivate var vkAppID = "6741241"
    
    fileprivate let fbPermissions = ["email","public_profile"]
    fileprivate let fbAppID = "312996479291631"
    
    
    weak var delegate: LoginServiceDelegate?
    
    override init() {
        super.init()
        let vkSDKInstance = VKSdk.initialize(withAppId: vkAppID)
        vkSDKInstance?.register(self)
        vkSDKInstance?.uiDelegate = self
    }
}

//MARK: VKLoginServiceInterface
extension SocialNetworkService: VKLoginServiceInterface {
    func loginVK() {
        VKSdk.wakeUpSession(vkScope as [AnyObject]) { (state, error) in
            if VKSdk.vkAppMayExists() {
                VKSdk.authorize(self.vkScope as [AnyObject])
            } else {
                VKSdk.authorize(self.vkScope as [AnyObject], with: .disableSafariController)
            }
        }
    }
    
    func logOutVK() {
        VKSdk.forceLogout()
        PlatformaUserDefaults.setSocialToken(nil)
    }
}

//MARK: - FacebookLoginServiceProtocol
extension SocialNetworkService: FBLoginServiceInterface {
    func loginFB() {
        if FBSDKAccessToken.current() != nil {
            PlatformaUserDefaults.setSocialToken(["fb" : FBSDKAccessToken.current().tokenString])
            self.delegate?.fbSdkAccessAuthorizationFinished()
        } else if let topVC = UIApplication.topViewController() {
            fbLoginManager.logIn(withReadPermissions: fbPermissions, from: topVC) { (result, error) in
                if error != nil || result == nil || (result?.isCancelled)! {
                    //self.delegate?.loginDataProviderHasError("Ошибка авторизации")
                } else {
                    PlatformaUserDefaults.setSocialToken(["fb" : FBSDKAccessToken.current().tokenString])
                    self.delegate?.fbSdkAccessAuthorizationFinished()
                }
            }
        }
    }
    
    func logOutFB() {
        fbLoginManager.logOut()
        PlatformaUserDefaults.setSocialToken(nil)
    }
}

//MARK: - VKSdkDelegate, VKSdkUIDelegate
extension SocialNetworkService: VKSdkDelegate, VKSdkUIDelegate {
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        guard result.error == nil else {
            //self.delegate?.loginDataProviderHasError("Ошибка авторизации")
            return
        }
        
        if result.token != nil {
            let token = ["vk": result.token.accessToken!]
            PlatformaUserDefaults.setSocialToken(token)
            self.delegate?.vkSdkAccessAuthorizationFinished()
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        //self.delegate?.loginDataProviderHasError("Ошибка авторизации")
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller, animated: true, completion: nil)
        }
    }
}


