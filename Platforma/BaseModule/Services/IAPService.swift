//
//  IAPService.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 12.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import StoreKit
import SwiftyStoreKit

protocol IAPServiceDelegate: class {
    func iAPServiceFinishedTransaction()
    func iAPServiceFinishedTransactionWithError()
}

class IAPService: NSObject {

    private override init() {}
    static let shared = IAPService()
    
    let sharedSecret = "b2738a2bb04b4e05a6e4a9eec7fe0075"
    
    let productIds: Set = [IAPProduct.oneMonthSub.rawValue,
                         IAPProduct.sixMonthSub.rawValue,
                         IAPProduct.twelveMonthSub.rawValue]
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    
    weak var delegate: IAPServiceDelegate?
    
    
    func getProducts() {
        for product in productIds {
            SwiftyStoreKit.retrieveProductsInfo([product]) { result in
                if let product = result.retrievedProducts.first {
                    let priceString = product.localizedPrice!
                    print("Product: \(product.localizedDescription), price: \(priceString)")
                }
                else if let invalidProductId = result.invalidProductIDs.first {
                    print("Invalid product identifier: \(invalidProductId)")
                }
                else {
                    print("Error: \(result.error)")
                }
            }
        }
    }
    
    func restorePurchases() {
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
                AlertWorker.showAlertWithOneButton(title: "Восстановление покупок", message: "Все покупки, связанные с данным AppleID, восстановлены", fromVC: UIApplication.topViewController()!)
            }
            else if results.restoredPurchases.count > 0 {
                print("Restore Success: \(results.restoredPurchases)")
                AlertWorker.showAlertWithOneButton(title: "Восстановление покупок", message: "Все покупки, связанные с данным AppleID, восстановлены", fromVC: UIApplication.topViewController()!)
            }
            else {
                AlertWorker.showAlertWithOneButton(title: "Восстановление покупок", message: "Покупки отсутствуют", fromVC: UIApplication.topViewController()!)
            }
            self.verifySubscriptions(productIds: self.productIds, sharedSecret: self.sharedSecret)
            self.delegate?.iAPServiceFinishedTransaction()
        }
    }
    
    //MARK: Subscription Verification
    
    func verifySubscriptions(productIds: Set<String>, sharedSecret: String, completion: ((VerifySubscriptionResult) -> Void)? = nil) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            var subValidationStatuses = [Bool]()
            switch result {
            case .success(let receipt):
                let productIds = productIds
                for product in productIds {
                    if product == IAPProduct.oneMonthSub.rawValue {
                        subValidationStatuses.append(self.verifyNonRenewableSub(productId: product, duration: 3600 * 24 * 30, receipt: receipt))
                    } else if product == IAPProduct.sixMonthSub.rawValue {
                        subValidationStatuses.append(self.verifyNonRenewableSub(productId: product, duration: 3600 * 24 * 183, receipt: receipt))
                    } else if product == IAPProduct.twelveMonthSub.rawValue {
                        subValidationStatuses.append(self.verifyNonRenewableSub(productId: product, duration: 3600 * 24 * 365, receipt: receipt))
                    }
                }
                if subValidationStatuses.contains(true) {
                    PlatformaUserDefaults.setUserPremiumStatus(status: true)
                } else {
                    PlatformaUserDefaults.setUserPremiumStatus(status: false)
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
                
                PlatformaUserDefaults.setUserPremiumStatus(status: false)
            }
        }
    }
    
    func purchaseSubscription(productId: String, sharedSecret: String) {
        let productId = productId
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            if case .success(let purchase) = result {
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                self.verifySubscriptions(productIds: self.productIds, sharedSecret: sharedSecret)
                self.delegate?.iAPServiceFinishedTransaction()
            } else {
                self.delegate?.iAPServiceFinishedTransaction()
            }
        }
    }
    
    private func verifyNonRenewableSub(productId: String, duration: TimeInterval, receipt: ReceiptInfo) -> Bool {
        let purchaseResult = SwiftyStoreKit.verifySubscription(ofType: SubscriptionType.nonRenewing(validDuration: duration), productId: productId, inReceipt: receipt)
        switch purchaseResult {
        case .purchased(let expiryDate, let items):
            print("\(productId) is valid until \(expiryDate)\n\(items)\n")
            
            return true
        case .expired(let expiryDate, let items):
            print("\(productId) is expired since \(expiryDate)\n\(items)\n")
            
            return false
        case .notPurchased:
            print("The user has never purchased \(productId)")
            
            return false
        }
    }
}

