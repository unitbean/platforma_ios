//
//  APIService.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
}


class APIService: MainScreenAPIServiceProtocol, PartnerSalesAPIServiceProtocol, DetailedEventAPIServiceProtocol, ChoosenEventsAPIServiceProtocol {
    
    
    private let fbFields = "id, name, link, first_name, last_name, picture.type(large), email, friends, friendlists"
    
    func convertResult<T: Decodable>(URLString: String, requestType: HTTPMethod, params: Data?, completion: @escaping (T?, String?) -> Void) {

        guard let reachbility = Reachability(),
            reachbility.currentReachabilityStatus != .notReachable else {
                completion(nil, "Подключение к сети отсутствует")
                return
        }
        
        guard let url = URL(string: GlobalConstants.host + URLString) else {return}
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 15)
        request.httpMethod = requestType.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if let appVersion = appVersion {
           request.addValue(appVersion, forHTTPHeaderField: "X-App-Version")
        }
        
        if let parameters = params {
            request.httpBody = parameters
            debugPrint("jsonDataParameters: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    debugPrint(error.localizedDescription)
                }
                
                if let response = response {
                    if let URL = response.url {
                        debugPrint("URL: \(URL)")
                    }
                    let httpResponse = response as? HTTPURLResponse
                    debugPrint("Status Code: \(httpResponse?.statusCode ?? 500)")
                }
                
                if let data = data {
                    let responseData = try? JSONSerialization.jsonObject(with: data, options: [])
                    debugPrint(responseData ?? "")
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let model = try decoder.decode(T.self, from: data)
                        completion(model, nil)
                    } catch let myDataError {
                        debugPrint(myDataError)
                        completion(nil, "Не удалось загрузить данные")
                    }
                } else {
                    completion(nil, "Не удалось загрузить данные")
                }
            }
        }.resume()
    }
}

