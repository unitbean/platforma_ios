//
//  DataStoresList.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


class DataStoresList {
    
    let mainScreenDataStore = MainScreenDataStore()
    
    let choosenEventsDataStore = ChoosenEventsDataStore()
    
    let partnerSalesDataStore = PartnerSalesDataStore()
    
    let userProfileDataStore = UserProfileDataStore()
    
    let detailedEventDataStore = DetailedEventDataStore()
    
    let detailedPromoDataStore = DetailedPromoDataStore()
}
