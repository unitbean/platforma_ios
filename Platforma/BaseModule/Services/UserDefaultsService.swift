//
//  UserDefaultsService.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 06.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


class PlatformaUserDefaults: NSObject {
    
    static func getChoosenEventsIds() -> [String]? {
        return UserDefaults.standard.object(forKey: "EVENTS_IDS") as? [String]
    }
    
    static func setChoosenEventId(id: String) {
        let defaults = UserDefaults.standard
        var choosenEvents: [String] = []
        if PlatformaUserDefaults.getChoosenEventsIds() == nil {
            choosenEvents.append(id)
            defaults.setValue(choosenEvents, forKey: "EVENTS_IDS")
            defaults.synchronize()
        } else if let eventsIds = PlatformaUserDefaults.getChoosenEventsIds(), !eventsIds.contains(id) {
            choosenEvents = eventsIds
            choosenEvents.append(id)
            defaults.setValue(choosenEvents, forKey: "EVENTS_IDS")
            defaults.synchronize()
        }
    }
    
    static func removeChoosenEventId(id: String) {
        let defaults = UserDefaults.standard
        if let eventsIds = PlatformaUserDefaults.getChoosenEventsIds(),
            let index = eventsIds.index(where: {$0 == id}) {
            var choosenEvents = eventsIds
            choosenEvents.remove(at: index)
            defaults.setValue(choosenEvents, forKey: "EVENTS_IDS")
            defaults.synchronize()
        }
    }
    
    static func setSocialToken(_ token: [String: String]?){
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "TOKEN")
        defaults.synchronize()
    }
    
    static func getSocialToken() -> NSDictionary? {
        return UserDefaults.standard.object(forKey: "TOKEN") as? NSDictionary
    }
    
    static func setUserPremiumStatus(status: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(status, forKey: "PREMIUM_STATUS")
        defaults.synchronize()
    }
    
    static func getUserPremiumStatus() -> Bool? {
        return UserDefaults.standard.bool(forKey: "PREMIUM_STATUS")
    }
}
