//
//  LoginView.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit
import EasyDi
import VK_ios_sdk



protocol LoginViewDelegate: class {
    func vkSdkAccessAuthorizationFinished()
    func fbSdkAuthorizationFinished()
}

class LoginView: UIView {
    
    @IBOutlet weak var facebookButtonView: UIView!
    @IBOutlet weak var vkButtonView: UIView!
    @IBOutlet weak var restorePurchasesButtonView: UIView!
    
    let loginService = DIAssembly.instance().socialNetworkService
    let activityIndicator = ActivityIndicator()
    weak var delegate: LoginViewDelegate?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureAll()
    }
    
    private func configureAll() {
        configureButtonSelectors()
        configureButtonViews()
        activityIndicator.setup(with: self, backgroundColor: UIColor.black.withAlphaComponent(0.5), indicatorColor: UIColor.white)
        loginService.delegate = self
        IAPService.shared.delegate = self
    }
    
    private func configureButtonSelectors() {
        let facebookTap = UITapGestureRecognizer(target: self, action: #selector(facebookDidTap))
        let vkTap = UITapGestureRecognizer(target: self, action: #selector(vkDidTap))
        let restorePurchasesTap = UITapGestureRecognizer(target: self, action: #selector(restorePurchasesDidTap))
        
        facebookButtonView.addGestureRecognizer(facebookTap)
        vkButtonView.addGestureRecognizer(vkTap)
        restorePurchasesButtonView.addGestureRecognizer(restorePurchasesTap)
    }
    
    private func configureButtonViews() {
        facebookButtonView.layer.cornerRadius = 10
        vkButtonView.layer.cornerRadius = 10
        restorePurchasesButtonView.layer.cornerRadius = 10
        
        facebookButtonView.isUserInteractionEnabled = true
        vkButtonView.isUserInteractionEnabled = true
        restorePurchasesButtonView.isUserInteractionEnabled = true
    }
    
    @objc func facebookDidTap() {
        loginService.loginFB()
    }
    
    @objc func vkDidTap() {
        loginService.loginVK()
    }
    
    @objc func restorePurchasesDidTap() {
        activityIndicator.show()
        IAPService.shared.restorePurchases()
    }
}


extension LoginView: LoginServiceDelegate {
    func fbSdkAccessAuthorizationFinished() {
        delegate?.fbSdkAuthorizationFinished()
    }
    
    func vkSdkAccessAuthorizationFinished() {
        delegate?.vkSdkAccessAuthorizationFinished()
    }
}

extension LoginView: IAPServiceDelegate {
    func iAPServiceFinishedTransaction() {
        activityIndicator.hide()
    }
    
    func iAPServiceFinishedTransactionWithError() {
        activityIndicator.hide()
    }
}
