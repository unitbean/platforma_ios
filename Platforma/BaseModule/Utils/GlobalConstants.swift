//
//  GlobalConstants.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit


class GlobalConstants {
    
    static let sharedInstance = GlobalConstants()
    
    static let host = "https://platforma.unitbean.ru/api/v1"
    
    static let picHost = "https://platforma.unitbean.ru/pics/"
    
}
