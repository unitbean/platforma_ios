//
//  CardPresentingAnimator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 02.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit


protocol CardPresentingAnimatorDelegate: class {
    func animatingDidEnd()
}


class CardPresentingAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let duration: Double
    
    private var expandedCell: EventCardCollectionViewCell?
    private var hiddenCells: [EventCardCollectionViewCell] = []
    
    weak var delegate: CardPresentingAnimatorDelegate?
    
    init(duration: Double) {
        self.duration = duration
    }
    
    
    
    private func collapseOrExpandCell(containerVC: MainScreenViewController, indexPath: IndexPath, collectionView: UICollectionView, completion: @escaping () -> ()) {
        
//        if collectionView.contentOffset.y < 0 ||
//            collectionView.contentOffset.y > collectionView.contentSize.height - collectionView.frame.height {
//            return
//        }
        
        let dampingRatio: CGFloat = 0.8
        let initialVelocity = CGVector.zero
        let springParameters = UISpringTimingParameters(dampingRatio: dampingRatio, initialVelocity: initialVelocity)
        let animator = UIViewPropertyAnimator(duration: duration, timingParameters: springParameters)
        
        containerVC.view.isUserInteractionEnabled = false
        
        if let selectedCell = expandedCell {
            containerVC.isStatusBarHidden = false
            if UIDevice.current.deviceModel == .iPhoneX {
                containerVC.view.constraints.filter{ $0.identifier == "collectionViewTopConstraint"}.first?.constant = 0
            }
            
            animator.addAnimations {
                selectedCell.collapse()
                self.showHideHeader(collectionView: collectionView)
                self.showTabBat(viewController: containerVC)
                if UIDevice.current.deviceModel == .iPhoneX {
                    containerVC.view.layoutIfNeeded()
                }
                
                for cell in self.hiddenCells {
                    cell.show()
                }
            }
            
            animator.addCompletion { _ in
                collectionView.isScrollEnabled = true
                
                self.expandedCell = nil
                self.hiddenCells.removeAll()
            }
        } else {
            containerVC.isStatusBarHidden = true
    
            collectionView.isScrollEnabled = false
            
            let selectedCell = collectionView.cellForItem(at: indexPath)! as! EventCardCollectionViewCell
            let frameOfSelectedCell = selectedCell.frame
            
            expandedCell = selectedCell
            hiddenCells = collectionView.visibleCells.map { $0 as! EventCardCollectionViewCell }.filter { $0 != selectedCell }
            if UIDevice.current.deviceModel == .iPhoneX {
                containerVC.view.constraints.filter{ $0.identifier == "collectionViewTopConstraint"}.first?.constant = -44
            }
            
            animator.addAnimations {
                self.hideTabBar(viewController: containerVC)
                if UIDevice.current.deviceModel == .iPhoneX {
                    containerVC.view.layoutIfNeeded()
                }
                self.showHideHeader(collectionView: collectionView)
                selectedCell.expand(in: collectionView)
                
                for cell in self.hiddenCells {
                    cell.hide(in: collectionView, frameOfSelectedCell: frameOfSelectedCell)
                }
            }
            
            animator.addCompletion { _ in
                collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(row: 0, section: 0))?.alpha = 0
            }
        }
        
        animator.addAnimations {
            containerVC.setNeedsStatusBarAppearanceUpdate()
        }
        
        animator.addCompletion { _ in
            containerVC.view.isUserInteractionEnabled = true
            completion()
        }
        
        animator.startAnimation()
    }
    
    private func showHideHeader(collectionView: UICollectionView) {
        
        let header = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(row: 0, section: 0))
        let headerAlpha = header?.alpha
        
        if headerAlpha == 1 {
            header?.alpha = 0
        } else {
            header?.alpha = 1
        }
    }
    
    private func hideTabBar(viewController: UIViewController) {
        if let tabBarHeight = viewController.tabBarController?.tabBar.frame.height {
          viewController.tabBarController?.tabBar.frame.origin.y += tabBarHeight
        }
    }
    
    private func showTabBat(viewController: UIViewController) {
        if let tabBarHeight = viewController.tabBarController?.tabBar.frame.height {
            viewController.tabBarController?.tabBar.frame.origin.y -= tabBarHeight
        }
    }
    
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        
        var navigationController: UINavigationController?
        
        if expandedCell != nil {
            navigationController = transitionContext.viewController(forKey: .to) as? UINavigationController
        } else {
            navigationController = transitionContext.viewController(forKey: .from) as? UINavigationController
        }
        
        guard let tabBarController = navigationController?.viewControllers[0] as? TabBarController,
            let fromVc = tabBarController.viewControllers?[0] as? MainScreenViewController else { return }
        guard let selectedCellIndex = fromVc.collectionView.indexPathsForSelectedItems else { return }
        guard let collectionView = fromVc.collectionView else { return }
        
        if self.expandedCell != nil {
            containerView.addSubview(toView)
            containerView.bringSubview(toFront: toView)
        }
        
        collapseOrExpandCell(containerVC: fromVc, indexPath: selectedCellIndex[0], collectionView: collectionView, completion: {
            if self.expandedCell != nil {
                containerView.addSubview(toView)
                containerView.bringSubview(toFront: toView)
                self.delegate?.animatingDidEnd()
            }
            transitionContext.completeTransition(true)
        })
        
    }
}
