//
//  ColorsConstants.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit


class ColorConstants {
    static let blackGradientColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.9)
    static let clearGradientColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
    static let adOrangeGradientColor = UIColor(red: 255.0/255.0, green: 147.0/255.0, blue: 101.0/255.0, alpha: 1.0)
    static let adVioletGradientColor = UIColor(red: 209.0/255.0, green: 84.0/255.0, blue: 170.0/255.0, alpha: 1.0)
    static let adBlueGradientColor = UIColor(red: 93.0/255.0, green: 119.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    static let adPurpleGrarientColor = UIColor(red: 171.0/255.0, green: 40.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    static let adDarkPurpleGradientColor = UIColor(red: 127.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let pink = UIColor(red: 255.0/255.0, green: 112.0/255.0, blue: 156.0/255.0, alpha: 1.0)
    static let cyan = UIColor(red: 7.0/255.0, green: 253.0/255.0, blue: 238.0/255.0, alpha: 1.0)
}
