//
//  IAPProduct.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 12.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


enum IAPProduct: String {
    case oneMonthSub    = "com.unitbean.Platforma.1monthSubscription"
    case sixMonthSub    = "com.unitbean.Platforma.6monthSubscription"
    case twelveMonthSub = "com.unitbean.Platforma.12monthSubscription"
}
