//
//  TabBarController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    

    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        createTabs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    
    private func createTabs() {
        let mainScreenVC = UIStoryboard(name: "MainScreen", bundle: nil).instantiateInitialViewController()
        let choosenEventsVC = UIStoryboard(name: "ChoosenEvents", bundle: nil).instantiateInitialViewController()
        let partnerSalesVC = UIStoryboard(name: "PartnerSales", bundle: nil).instantiateInitialViewController()
        let userProfileVC = UIStoryboard(name: "UserProfile", bundle: nil).instantiateInitialViewController()
        
        guard let mainScreen = mainScreenVC, let choosenEvents = choosenEventsVC, let partnerSales = partnerSalesVC,
                let userProfile = userProfileVC else { return }
        
        let tabBarList = [mainScreen, choosenEvents, partnerSales, userProfile]
        
        customizeTabBar(tabs: tabBarList)
        
        viewControllers = tabBarList
    }
    
    private func customizeTabBar(tabs: [UIViewController]) {
        let mainNotActiveIcon = UIImage(named: "icMainNotActive")?.withRenderingMode(.alwaysOriginal)
        let mainActiveIcon = UIImage(named: "icMainActive")?.withRenderingMode(.alwaysOriginal)
        let choosenEventsNotActiveIcon = UIImage(named: "icCalendarNotActive")?.withRenderingMode(.alwaysOriginal)
        let choosenEventsActiveIcon = UIImage(named: "icCalendarActive")?.withRenderingMode(.alwaysOriginal)
        let partnerSalesNotActiveIcon = UIImage(named: "giftNotActive")?.withRenderingMode(.alwaysOriginal)
        let partnerSalesActiveIcon = UIImage(named: "giftActive")?.withRenderingMode(.alwaysOriginal)
        let userProfileNotActiveIcon = UIImage(named: "profileNotActive")?.withRenderingMode(.alwaysOriginal)
        let userProfileActiveIcon = UIImage(named: "profileActive")?.withRenderingMode(.alwaysOriginal)
        
        tabs[0].tabBarItem = UITabBarItem(title: nil, image: mainNotActiveIcon, selectedImage: mainActiveIcon)
        tabs[1].tabBarItem = UITabBarItem(title: nil, image: choosenEventsNotActiveIcon, selectedImage: choosenEventsActiveIcon)
        tabs[2].tabBarItem = UITabBarItem(title: nil, image: partnerSalesNotActiveIcon, selectedImage: partnerSalesActiveIcon)
        tabs[3].tabBarItem = UITabBarItem(title: nil, image: userProfileNotActiveIcon, selectedImage: userProfileActiveIcon)
    }
}

