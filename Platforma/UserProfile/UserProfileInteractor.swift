//
//  UserProfileUserProfileInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import EasyDi

/// Класс для описания бизнес-логики модуля UserProfile
class UserProfileInteractor: UserProfileInteractorInput {

    private let presenter: UserProfilePresenterInput
    private let dataprovider: UserProfileDataProviderInput
    private let facebookLoginService: FBLoginServiceInterface
    private let vkLoginService: VKLoginServiceInterface

    required init(socialNetworkService: SocialNetworkService, dataProvider: UserProfileDataProvider, presenter: UserProfilePresenter) {
        self.presenter = presenter
        self.dataprovider = dataProvider
        self.facebookLoginService = socialNetworkService
        self.vkLoginService = socialNetworkService
    }

    func showUserProfile(request: UserProfile.ShowProfile.Request) {
        if let token = PlatformaUserDefaults.getSocialToken() {
            dataprovider.getUserData(token: token) { [weak self] (userModel, error) in
                let result: UserProfile.ShowProfile.RequestResult
                if let userModel = userModel {
                    self?.dataprovider.storeUserData(model: UserProfileModel(socialUserModel: userModel))
                    result = .success(UserProfileModel(socialUserModel: userModel))
                    self?.presenter.presentUserProfile(response: UserProfile.ShowProfile.Response(result: result))
                } else {
                    result = .failure("Not authorized")
                    self?.dataprovider.storeUserData(model: nil)
                    self?.presenter.presentUserProfile(response: UserProfile.ShowProfile.Response(result: result))
                }
            }
        } else {
            let result: UserProfile.ShowProfile.RequestResult
            result = .failure("Not authorized")
            self.presenter.presentUserProfile(response: UserProfile.ShowProfile.Response(result: result))
        }
    }
    
    func logOut() {
        vkLoginService.logOutVK()
        facebookLoginService.logOutFB()
        let result: UserProfile.ShowProfile.RequestResult = .failure("Not authorized")
        self.presenter.presentUserProfile(response: UserProfile.ShowProfile.Response(result: result))
    }
}
