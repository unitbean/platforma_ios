//
//  UserProfileUserProfileRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class UserProfileRouter: UserProfileRouterProtocol {

    var dataStoresList: /*App name*/DataStoresList

    func routeToSubPurchase(fromVc: UserProfileViewController) {
        let toVc = UIStoryboard(name: "SubPurchase", bundle: nil).instantiateInitialViewController() as! SubPurchaseViewController
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        toVc.delegate = fromVc
        DispatchQueue.main.async {
            fromVc.navigationController?.present(toVc, animated: true, completion: nil)
        }
    }




    init(dataStoresList: /*App name*/DataStoresList) {
        self.dataStoresList = dataStoresList
    }
}
