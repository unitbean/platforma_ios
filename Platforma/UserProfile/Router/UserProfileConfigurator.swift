//
//  UserProfileUserProfileConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class UserProfileConfigurator {

    func configureModule(viewController: UserProfileViewController) {
        let presenter = UserProfilePresenter(viewController: viewController)
        let apiService = DIAssembly.instance().api
        let socialNetworkService = DIAssembly.instance().socialNetworkService
        let dataStore = DIAssembly.instance().dataStoresList.userProfileDataStore
        let dataProvider = UserProfileDataProvider(dataStore: dataStore, apiService: apiService)
        let interactor = UserProfileInteractor(socialNetworkService: socialNetworkService, dataProvider: dataProvider, presenter: presenter)
        let router = UserProfileRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
