//
//  UserProfileUserProfilePresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class UserProfilePresenter: UserProfilePresenterInput {

    weak var viewController: UserProfileViewControllerInput!

    required init(viewController: UserProfileViewController) {
        self.viewController = viewController
    }

    func presentUserProfile(response: UserProfile.ShowProfile.Response) {
        var viewModel = UserProfile.ShowProfile.ViewModel(state: .notAuthorized)

        switch response.result {
        case let .failure(error):
            if error == "Not authorized" {
                viewModel = UserProfile.ShowProfile.ViewModel(state: .notAuthorized)
            }
        case let .success(result):
            var profileViewModel = UserProfileViewModel()
            profileViewModel.avatar = result.socialUserModel?.avatar
            profileViewModel.name = result.socialUserModel?.name
            profileViewModel.lastName = result.socialUserModel?.lastName
            if let premiumStatus = PlatformaUserDefaults.getUserPremiumStatus() {
                profileViewModel.isPremium = premiumStatus
            } else {
                profileViewModel.isPremium = false
            }
            viewModel = UserProfile.ShowProfile.ViewModel(state: .result(profileViewModel))
        }
        viewController.displayUserProfile(viewModel: viewModel)
    }
}
