//
//  UserProfileUserProfileDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum UserProfile {
    // MARK: Use cases
    enum ShowProfile {
        struct Request {
        }

        struct Response {
            var result: UserProfile.ShowProfile.RequestResult
        }

        struct ViewModel {
            var state: ViewControllerState
        }
        
        enum ViewControllerState {
            case result(UserProfileViewModel)
            case notAuthorized
            case emptyResult
            case error(message: String)
        }
        
        enum RequestResult {
            case failure(String)
            case success(UserProfileModel)
        }
    }
}


