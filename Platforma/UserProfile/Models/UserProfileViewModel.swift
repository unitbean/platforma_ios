//
//  UserProfileUserProfileViewModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct UserProfileViewModel {
    var name: String?
    var lastName: String?
    var avatar: String?
    var isPremium: Bool?
}
