//
//  UserProfileUserProfileDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля UserProfile
struct UserProfileDataProvider: UserProfileDataProviderInput {
    
    let dataStore: UserProfileDataStore
    let vkApiService: VKUserDataAPIServiceProtocol
    let fbApiService: FBUserDataAPIServiceProtocol

    init(dataStore: UserProfileDataStore, apiService: APIService) {
        self.dataStore = dataStore
        self.vkApiService = apiService
        self.fbApiService = apiService
    }

    // Предоставление данных согласно определенным Use cases
    
    func getUserData(token: NSDictionary, completion: @escaping (SocialUserServiceModel?, String?) -> Void) {
        if let vkToken = token["vk"] {
            vkApiService.getVKUserModels(token: vkToken as! String) { (userModel) in
                if userModel != nil {
                    completion(userModel, nil)
                } else {
                    completion(nil, "Ошибка получения данных пользователя")
                }
            }
        } else if token["fb"] != nil {
            fbApiService.getFacebookPersonalInformation { (userModel) in
                if userModel != nil {
                    completion(userModel, nil)
                } else {
                    completion(nil, "Ошибка получения данных пользователя")
                }
            }
        }
    }
    func storeUserData(model: UserProfileModel?) {
        dataStore.mainModel = model
    }
}
