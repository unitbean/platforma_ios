//
//  UserProfileUserProfileViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let configurator = UserProfileConfigurator()
    var interactor: UserProfileInteractorInput!
    var router: UserProfileRouterProtocol!
    
    private let cellIdentifiers = [String.init(describing: UserProfileNameTableViewCell.self),
                                   String.init(describing: UserProfileAdTableViewCell.self),
                                   String.init(describing: UserProfileActionTableViewCell.self)]
    
    fileprivate var activityIndicator = ActivityIndicator()
    
    fileprivate let tableViewDataSource = UserProfileTableViewDataSource()
    fileprivate let scrollView: UIScrollView = {
        let view = UIScrollView()
        //view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private var loginView: LoginView = Bundle.main.loadNibNamed("LoginView", owner: nil, options: nil)?.first as! LoginView
    private var state = UserProfile.ShowProfile.ViewControllerState.notAuthorized
    private var isPremium: Bool? {
        willSet {
            if isPremium != newValue {
                tableView.reloadData()
            }
        }
    }



    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configureModule(viewController: self)
        activityIndicator.setup(with: self.view, backgroundColor: UIColor.white)
        showProgress()
        setupSubViews()
        displayInitial()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        setupNavigationBar()
        
        self.isPremium = PlatformaUserDefaults.getUserPremiumStatus()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        
        //MARK: - REFACTOR
        // small screen device check
        if view.bounds.height <= 570 {
            loginView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        } else {
            loginView.frame = CGRect(x: 0, y: view.safeAreaInsets.top, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        }
    }
    
    //MARK: - Private methods
    
    private func displayInitial() {
        interactor.showUserProfile(request: UserProfile.ShowProfile.Request())
    }
    
    private func setupSubViews() {
        setupTableView()
        setupLoginView()
    }
    
    private func setupLoginView() {
        //MARK: - REFACTOR
        // small screen device check
        if view.bounds.height <= 570 {
            scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height + 1)
        } else {
            scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
        }
        
        scrollView.backgroundColor = UIColor(red: 242.0/255.0, green: 240.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        self.view.backgroundColor = UIColor(red: 242.0/255.0, green: 240.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        debugPrint(view.bounds.height)
        
        self.view.addSubview(scrollView)
        loginView.backgroundColor = UIColor(red: 242.0/255.0, green: 240.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        scrollView.addSubview(loginView)
        loginView.isHidden = true
        
        loginView.delegate = self
    }
    
    private func setupTableView() {
        for identifier in cellIdentifiers {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDataSource
        tableViewDataSource.vc = self
    }
    
    private func setupNavigationBar() {
        self.parent?.title = "Профиль"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = true
    }
}

//MARK: - UserProfileViewControllerInput
extension UserProfileViewController: UserProfileViewControllerInput {
    func displayUserProfile(viewModel: UserProfile.ShowProfile.ViewModel) {
        state = viewModel.state
        switch state {
        case let .error(message):
            print("error \(message)")
            hideProgress()
        case let .result(viewModel):
            self.state = .result(viewModel)
            tableViewDataSource.viewModel = viewModel
            updateTableViewData()
            showTable()
            hideProgress()
        case .notAuthorized:
            self.state = .notAuthorized
            displayNotAuthorized()
            hideProgress()
        default: break
        }
    }
}

//MARK: - UserProfileViewInput
extension UserProfileViewController: UserProfileViewInput {

    func showTable() {
        loginView.isHidden = true
        tableView.isHidden = false
        scrollView.isHidden = true
    }

    func displayNotAuthorized() {
        tableView.isHidden = true
        scrollView.isHidden = false
        loginView.isHidden = false
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        // TODO: - Отображение ошибки соединения
    }

    func hideConnectionError() {
        // TODO: - Скрыть отображение ошибки соединения
    }

    func showProgress() {
        activityIndicator.show()
    }

    func hideProgress() {
        activityIndicator.hide()
    }

    func updateTableViewData() {
        self.tableView.tableHeaderView = nil
        self.tableView.dataSource = self.tableViewDataSource
        self.tableView.delegate = self.tableViewDataSource
        self.tableView.reloadData()
    }
}

//MARK: - LoginViewDelegate
extension UserProfileViewController: LoginViewDelegate {
    func fbSdkAuthorizationFinished() {
        interactor.showUserProfile(request: UserProfile.ShowProfile.Request())
    }
    
    func vkSdkAccessAuthorizationFinished() {
        interactor.showUserProfile(request: UserProfile.ShowProfile.Request())
    }
}

//MARK: - SubPurchaseViewControllerDelegate
extension UserProfileViewController: SubPurchaseViewControllerDelegate {
    func reloadData() {
        tableView.reloadSections([0], with: UITableViewRowAnimation.fade)
    }
}












