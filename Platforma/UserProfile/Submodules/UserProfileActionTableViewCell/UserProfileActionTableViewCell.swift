//
//  UserProfileActionTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class UserProfileActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellActionLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func customize(rowIndex: Int) {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            switch rowIndex {
            case 1:
                cellActionLabel.text = "Уведомления"
                switchButton.isHidden = false
            case 2:
                cellActionLabel.text = "Восстановить покупки"
                switchButton.isHidden = true
            case 3:
                cellActionLabel.text = "Выйти"
                switchButton.isHidden = true
            default: break
            }
        } else {
            switch rowIndex {
            case 2:
                cellActionLabel.text = "Уведомления"
                switchButton.isHidden = false
            case 3:
                cellActionLabel.text = "Восстановить покупки"
                switchButton.isHidden = true
            case 4:
                cellActionLabel.text = "Выйти"
                switchButton.isHidden = true
            default: break
            }
        }
    }
}
