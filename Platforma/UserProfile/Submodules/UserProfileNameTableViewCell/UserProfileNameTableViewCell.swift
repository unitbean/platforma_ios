//
//  UserProfileNameTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class UserProfileNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var viewModel: UserProfileViewModel? {
        didSet {
            if let name = viewModel?.name, let lastName = viewModel?.lastName {
                userNameLabel.text = name + " " + lastName
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        avatarImageView.layer.masksToBounds = true
    }
    
}
