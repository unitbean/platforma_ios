//
//  UserProfileAdTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 11.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class UserProfileAdTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    var viewHasGradient: Bool = false
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureAll()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        if !viewHasGradient {
            addGradient()
            viewHasGradient = true
        }
    }

    private func configureAll() {
        configureView()
    }
    
    private func configureView() {
        containerView.layer.cornerRadius = 14
        
        self.contentView.backgroundColor = .white
        self.contentView.layer.masksToBounds = true
        
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 12.0/255.0, alpha: 0.12).cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 22)
        containerView.layer.shadowRadius = 22
    }
    
    private func addGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = containerView.bounds
        gradient.colors = [ColorConstants.adDarkPurpleGradientColor.cgColor,
                           ColorConstants.adPurpleGrarientColor.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        gradient.cornerRadius = 14
        
        containerView.layer.insertSublayer(gradient, at: 0)
    }
}
