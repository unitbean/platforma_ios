//
//  UserProfileUserProfileTableViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit



class UserProfileTableViewDataSource: NSObject, UITableViewDataSource {

    var viewModel: UserProfileViewModel?

    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc: UserProfileViewController!

    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            return 4
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserProfileNameTableViewCell.self), for: indexPath) as! UserProfileNameTableViewCell
                if let url = viewModel?.avatar {
                    cell.avatarImageView.kf.setImage(with: URL(string: url))
                }
                cell.viewModel = viewModel
                return cell
            case 1, 2, 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserProfileActionTableViewCell.self), for: indexPath) as! UserProfileActionTableViewCell
                cell.customize(rowIndex: indexPath.row)
                return cell
            default:
                return UITableViewCell()
            }
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserProfileNameTableViewCell.self), for: indexPath) as! UserProfileNameTableViewCell
                cell.viewModel = viewModel
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserProfileAdTableViewCell.self), for: indexPath) as! UserProfileAdTableViewCell
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
                return cell
            case 2, 3, 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserProfileActionTableViewCell.self), for: indexPath) as! UserProfileActionTableViewCell
                cell.customize(rowIndex: indexPath.row)
                return cell
            default:
                return UITableViewCell()
            }
        }
    }
}


extension UserProfileTableViewDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            return UITableViewAutomaticDimension
        } else {
            if indexPath.row == 1 {
                return 200
            }
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            switch indexPath.row {
            case 2:
                IAPService.shared.restorePurchases()
                
            case 3:
                vc.interactor.logOut()
            default: break
            }
        } else {
            switch indexPath.row {
            case 1:
                vc.router.routeToSubPurchase(fromVc: vc)
            case 3:
                IAPService.shared.restorePurchases()
            case 4:
                vc.interactor.logOut()
            default: break
            }
        }
    }
}













