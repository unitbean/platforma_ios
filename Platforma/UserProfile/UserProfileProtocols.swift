//
//  UserProfileUserProfileProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit

/// UserProfile DataProvider Interface
protocol UserProfileDataProviderInput {
    func getUserData(token: NSDictionary, completion: @escaping (SocialUserServiceModel?, String?) -> Void)
    func storeUserData(model: UserProfileModel?)
}

/// UserProfile Router Interface
protocol UserProfileRouterProtocol {
    func routeToSubPurchase(fromVc: UserProfileViewController)
}

/// UserProfile ViewController Interface
protocol UserProfileViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayUserProfile(viewModel: UserProfile.ShowProfile.ViewModel)
}

/// UserProfile View Interface
protocol UserProfileViewInput: class {
    func displayNotAuthorized()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateTableViewData()
}

/// UserProfile Interactor Interface
protocol UserProfileInteractorInput {
    func showUserProfile(request: UserProfile.ShowProfile.Request)
    func logOut()
}

/// UserProfile Presenter Interface
protocol UserProfilePresenterInput: class {
    func presentUserProfile(response: UserProfile.ShowProfile.Response)
}






