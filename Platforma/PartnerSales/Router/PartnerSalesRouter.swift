//
//  PartnerSalesPartnerSalesRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class PartnerSalesRouter: PartnerSalesRouterProtocol {

    var dataStoresList: DataStoresList

    func routeToDetailedPromo(fromVc: PartnerSalesViewController, modelIndex: Int) {
        let fromVcDataStore = dataStoresList.partnerSalesDataStore
        guard let partnerSales = fromVcDataStore.partnerSales, let sales = partnerSales.partners, !sales.isEmpty else { return }
        dataStoresList.detailedPromoDataStore.promoModel = sales[modelIndex]
        let toVc = UIStoryboard(name: "DetailedPromo", bundle: nil).instantiateInitialViewController() as! DetailedPromoViewController
        //toVc.delegate = fromVc
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        toVc.modalPresentationCapturesStatusBarAppearance = true
        fromVc.present(toVc, animated: true) {
            toVc.showCloseButton()
        }
    }
    
    func routeToSubPurchase(fromVc: PartnerSalesViewController) {
        let toVc = UIStoryboard(name: "SubPurchase", bundle: nil).instantiateInitialViewController() as! SubPurchaseViewController
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        fromVc.navigationController?.present(toVc, animated: true, completion: nil)
    }


    
    init(dataStoresList: /*App name*/DataStoresList) {
        self.dataStoresList = dataStoresList
    }
}
