//
//  PartnerSalesPartnerSalesConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class PartnerSalesConfigurator {

    func configureModule(viewController: PartnerSalesViewController) {
        let apiService = DIAssembly.instance().api
        let dataStore = DIAssembly.instance().dataStoresList.partnerSalesDataStore
        let presenter = PartnerSalesPresenter(viewController: viewController)
        let dataProvider = PartnerSalesDataProvider(dataStore: dataStore, apiService: apiService)
        let interactor = PartnerSalesInteractor(dataProvider: dataProvider, presenter: presenter)
        let router = PartnerSalesRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
