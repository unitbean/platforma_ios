//
//  PartnerSalesPartnerSalesPresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class PartnerSalesPresenter: PartnerSalesPresenterInput {

    weak var viewController: PartnerSalesViewControllerInput!

    required init(viewController: PartnerSalesViewController) {
        self.viewController = viewController
    }
    
    func presentPartnerSales(response: PartnerSales.ShowPartners.Response) {
        var viewModel = PartnerSales.ShowPartners.ViewModel(state: .emptyResult)
        
        switch response.result {
        case let .failure(error):
            viewModel = PartnerSales.ShowPartners.ViewModel(state: .error(message: error))
        case let .success(result):
            if let partners = result.partners {
                if partners.isEmpty {
                    viewModel = PartnerSales.ShowPartners.ViewModel(state: .emptyResult)
                } else {
                    let mapped = result.partners!.map {
                        PartnerSalesViewModel(title            : $0.title,
                                              text          : $0.previewText,
                                              pic           : $0.mainPic,
                                              logo          : $0.logo
                        )
                    }
                    viewModel = PartnerSales.ShowPartners.ViewModel(state: .result(mapped))
                }
            }
        }
        viewController.displayPartnerSales(viewModel: viewModel)
    }
}
