//
//  PartnerSaleModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 08.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct PartnerSaleModel: Decodable {
    var id          : String?
    var title       : String?
    var description : String?
    var previewText : String?
    var previewPic  : String?
    var mainPic     : String?
    var logo        : String?
    var promoCode   : String?
}
