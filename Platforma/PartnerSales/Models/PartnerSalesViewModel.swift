//
//  PartnerSalesPartnerSalesViewModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct PartnerSalesViewModel {
    var title       : String?
    var text        : String?
    var pic         : String?
    var logo        : String?
}
