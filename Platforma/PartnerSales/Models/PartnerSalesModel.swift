//
//  PartnerSalesPartnerSalesModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct PartnerSalesModel: Decodable {
    var partners: [PartnerSaleModel]?
    
    private enum CodingKeys: String, CodingKey {
        case partners = "result"
    }
}
