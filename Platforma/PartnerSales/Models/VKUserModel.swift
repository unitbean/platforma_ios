//
//  VKUserModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 15.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


struct VKBaseResponse: Decodable {
    var response: [VKUserModel]?
    var error: VKUserError?
}

struct VKUserModel: Decodable {
    var id          : Int?
    var first_name  : String?
    var last_name   : String?
    var photo_200   : String?
}

struct VKUserError: Decodable {
    var error_code      : Int
    var error_message   : String
}
