//
//  PartnerSalesPartnerSalesViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class PartnerSalesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyResultPlaceholderView: UIView!
    
    let collectionViewDataSource = PartnerSalesCollectionViewDataSource()
    let cellIdentifier = String(describing: PartnerSaleCollectionViewCell.self)
    
    let configurator = PartnerSalesConfigurator()
    var interactor: PartnerSalesInteractorInput!
    var router: PartnerSalesRouterProtocol!

    var state = PartnerSales.ShowPartners.ViewControllerState.loading

    var isStatusBarHidden = false
    override var prefersStatusBarHidden: Bool {
        return isStatusBarHidden
    }
    
   
   

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configureModule(viewController: self)
        setupCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        emptyResultPlaceholderView.isHidden = true
        setupNavigationBar()
        displayInitial()
    }
    
    
    //MARK: - Private Methods
    private func displayInitial() {
        showProgress()
        interactor.loadPartnerSales(request: PartnerSales.ShowPartners.Request())
    }
    
    private func setupCollectionView() {
        collectionViewDataSource.vc = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collectionView.alwaysBounceVertical = true
        collectionView.contentInset = UIEdgeInsetsMake(10, 0, 20, 0)
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDataSource
    }
    
    private func setupNavigationBar() {
        self.parent?.title = "Партнеры"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = true
    }
}

//MARK: - PartnerSalesViewControllerInput
extension PartnerSalesViewController: PartnerSalesViewControllerInput {
    func routeToDetailedPromo(modelIndex: Int) {
        router.routeToDetailedPromo(fromVc: self, modelIndex: modelIndex)
    }
    
    func displayPartnerSales(viewModel: PartnerSales.ShowPartners.ViewModel) {
        state = viewModel.state
        switch state {
        case let .error(message):
            displayConnectionError()
            hideProgress()
        case let .result(partnerViewModel):
            collectionViewDataSource.viewModels = partnerViewModel
            updateCollectionViewData()
            hideConnectionError()
            hideProgress()
        case .emptyResult:
            print("empty result")
        default: break
        }
    }
}

//MARK: - PartnerSalesViewInput
extension PartnerSalesViewController: PartnerSalesViewInput {

    func showCollection() {
        collectionView.isHidden = false
    }

    func displayNoResult() {
        // TODO: - Отображение отсутствия результата
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        collectionView.isHidden = true
        emptyResultPlaceholderView.isHidden = false
    }

    func hideConnectionError() {
        emptyResultPlaceholderView.isHidden = true
    }

    func showProgress() {

    }

    func hideProgress() {
        
    }

    func updateCollectionViewData() {
        DispatchQueue.main.async {
            self.showCollection()
            self.collectionView.reloadSections([0])
        }
    }
}














