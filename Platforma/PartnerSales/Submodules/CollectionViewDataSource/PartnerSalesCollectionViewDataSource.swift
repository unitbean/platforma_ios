//
//  PartnerSalesPartnerSalesTableViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit


class PartnerSalesCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var viewModels: [PartnerSalesViewModel] = []

    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc: PartnerSalesViewController!

//    /// Используется для сообщения ViewController о взаимодействии с tableView
//    weak var delegate: PartnerSalesTableViewDataSourceDelegate?
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PartnerSaleCollectionViewCell.self), for: indexPath) as! PartnerSaleCollectionViewCell
        if let backgroundImageUrl = viewModels[indexPath.row].pic, let logoImageUrl = viewModels[indexPath.row].logo {
            cell.backgroundImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + backgroundImageUrl))
            cell.logoImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + logoImageUrl))
        }
        cell.viewModel = viewModels[indexPath.row]
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
}

//MARK: -
extension PartnerSalesCollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 1.0, bottom: 1.0, right: 1.0)
    }
}

//MARK: - UICollectionViewDelegate
extension PartnerSalesCollectionViewDataSource: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if PlatformaUserDefaults.getUserPremiumStatus() == true {
            vc.routeToDetailedPromo(modelIndex: indexPath.row)
        } else {
            vc.router.routeToSubPurchase(fromVc: vc)
        }
    }
}














