//
//  PartnerSaleCollectionViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 08.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit


class PartnerSaleCollectionViewCell: UICollectionViewCell, CardCollectionViewCellProtocol {
    
    var viewModel: PartnerSalesViewModel? {
        didSet {
            titleLabel.text = viewModel?.title
            text.text = viewModel?.text
        }
    }
    var viewHasGradient: Bool = false
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureAll()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        if !viewHasGradient {
            let firstColor = ColorConstants.clearGradientColor
            let secondColor = ColorConstants.blackGradientColor
            backgroundImageView.applyGradientToView(colors: [firstColor, secondColor])
            self.viewHasGradient = true
        }
    }
    
    // MARK: - Configuration
    private func configureAll() {
        configureCell()
        borderView.layer.cornerRadius = borderView.frame.width / 2
        logoImageView.layer.cornerRadius = logoImageView.frame.width / 2
        logoImageView.layer.masksToBounds = true
    }
    
}
