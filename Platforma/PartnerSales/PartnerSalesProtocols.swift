//
//  PartnerSalesPartnerSalesProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit

/// PartnerSales DataProvider Interface
protocol PartnerSalesDataProviderInput {
    func getPartnerSales(completion: @escaping (PartnerSalesModel?, String?) -> Void)
}

/// PartnerSales Router Interface
protocol PartnerSalesRouterProtocol {
    func routeToDetailedPromo(fromVc: PartnerSalesViewController, modelIndex: Int)
    func routeToSubPurchase(fromVc: PartnerSalesViewController)
}

/// PartnerSales ViewController Interface
protocol PartnerSalesViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayPartnerSales(viewModel: PartnerSales.ShowPartners.ViewModel)
}

/// PartnerSales View Interface
protocol PartnerSalesViewInput: class {
    func displayNoResult()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateCollectionViewData()
}

/// PartnerSales Interactor Interface
protocol PartnerSalesInteractorInput {
    func loadPartnerSales(request: PartnerSales.ShowPartners.Request)
}

/// PartnerSales Presenter Interface
protocol PartnerSalesPresenterInput: class {
    func presentPartnerSales(response: PartnerSales.ShowPartners.Response)
}






