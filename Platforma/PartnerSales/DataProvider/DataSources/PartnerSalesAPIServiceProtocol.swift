//
//  PartnerSalesPartnerSalesAPIServiceProtocol.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

protocol PartnerSalesAPIServiceProtocol: class {
    func getPartnerSales(completion: @escaping (PartnerSalesModel?, String?) -> Void)
}


extension PartnerSalesAPIServiceProtocol where Self: APIService {
    func getPartnerSales(completion: @escaping (PartnerSalesModel?, String?) -> Void) {
        let urlString = "/partner"
        convertResult(URLString: urlString, requestType: HTTPMethod.GET, params: nil, completion: completion)
    }
}



