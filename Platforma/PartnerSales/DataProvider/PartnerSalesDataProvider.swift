//
//  PartnerSalesPartnerSalesDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля PartnerSales
struct PartnerSalesDataProvider: PartnerSalesDataProviderInput {
    let dataStore: PartnerSalesDataStore
    let apiService: PartnerSalesAPIServiceProtocol

    init(dataStore: PartnerSalesDataStore, apiService: PartnerSalesAPIServiceProtocol) {
        self.dataStore = dataStore
        self.apiService = apiService
    }

    // Предоставление данных согласно определенным Use cases

    func getPartnerSales(completion: @escaping (PartnerSalesModel?, String?) -> Void) {
        apiService.getPartnerSales { (model, error) in
            if let error = error {
                completion(nil, error)
            } else if let model = model {
                self.dataStore.partnerSales = model
                completion(self.dataStore.partnerSales, nil)
            }
        }
    }
}
