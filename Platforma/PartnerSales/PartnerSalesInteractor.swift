//
//  PartnerSalesPartnerSalesInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// Класс для описания бизнес-логики модуля PartnerSales
class PartnerSalesInteractor: PartnerSalesInteractorInput {
    
    let presenter: PartnerSalesPresenterInput
    let dataProvider: PartnerSalesDataProviderInput

    required init(dataProvider: PartnerSalesDataProvider, presenter: PartnerSalesPresenter) {
        self.dataProvider = dataProvider
        self.presenter = presenter
    }

    func loadPartnerSales(request: PartnerSales.ShowPartners.Request) {
        dataProvider.getPartnerSales { [unowned self] (model, error) in
            let result: PartnerSales.ShowPartners.RequestResult
            if let model = model {
                result = .success(model)
            } else if let error = error {
                result = .failure(error)
            } else {
                result = .failure("Ошибка получения данных")
            }
            self.presenter.presentPartnerSales(response: PartnerSales.ShowPartners.Response(result: result))
        }
    }
}
