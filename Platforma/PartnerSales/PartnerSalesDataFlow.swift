//
//  PartnerSalesPartnerSalesDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum PartnerSales {
    // MARK: Use cases
    enum ShowPartners {
        struct Request {
        }

        struct Response {
            var result: PartnerSales.ShowPartners.RequestResult
        }

        struct ViewModel {
            var state: ViewControllerState
        }
        
        enum ViewControllerState {
            case loading
            case result([PartnerSalesViewModel])
            case emptyResult
            case error(message: String)
        }
        
        enum RequestResult {
            case failure(String)
            case success(PartnerSalesModel)
        }
    }
}


