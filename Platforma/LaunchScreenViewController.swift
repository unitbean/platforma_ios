//
//  LaunchScreenViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit
import EasyDi
import SwiftyStoreKit

/*
 Duplicate of LaunchScreen,
 extended with slide show and logo changing
 */

class LaunchScreenViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    private let backgroundImages: [UIImage?] = [ UIImage(named: "1"),
                                        UIImage(named: "2"),
                                        UIImage(named: "3")
                                      ]
    private let whiteLogoImage = UIImage(named: "logoWhite")
    
    private var timer: Timer?
    private var iteration = 0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(changeImages), userInfo: nil, repeats: true)
        IAPService.shared.verifySubscriptions(productIds: IAPService.shared.productIds, sharedSecret: IAPService.shared.sharedSecret) 
    }
    
    @objc
    private func changeImages() {
        
        if iteration == 0 {
            changeLogoImage()
        }
        if iteration <= backgroundImages.count - 1 {
            changeBackgroundImage()
            iteration += 1
        } else {
            timer?.invalidate()
            routeToMainScreen()
        }
    }
    
    private func changeLogoImage() {
        UIView.transition(with: self.logoImageView,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.logoImageView.image = self.whiteLogoImage },
                          completion: nil)
    }
    
    private func changeBackgroundImage() {
        UIView.transition(with: self.backgroundImageView,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.backgroundImageView.image = self.backgroundImages[self.iteration] },
                          completion: nil)
    }
    
    private func routeToMainScreen() {
        let tabBarController = TabBarController()
        let navigationController = UINavigationController(rootViewController: TabBarController())
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
}

