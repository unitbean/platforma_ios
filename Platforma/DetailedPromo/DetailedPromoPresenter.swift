//
//  DetailedPromoDetailedPromoPresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class DetailedPromoPresenter: DetailedPromoPresenterInput {

    weak var viewController: DetailedPromoViewControllerInput!

    required init(viewController: DetailedPromoViewController) {
        self.viewController = viewController
    }

    func presentDetailedPromo(response: DetailedPromo.ShowPromo.Response) {
        var viewModel: DetailedPromo.ShowPromo.ViewModel

        switch response.result {
        case let .failure(error):
            viewModel = DetailedPromo.ShowPromo.ViewModel(state: .error(message: error))
        case let .success(result):
            let mapped = DetailedPromoViewModel(title            : result.title,
                                                description   : result.description,
                                                text          : result.previewText,
                                                promoCode     : result.promoCode,
                                                pic           : result.mainPic,
                                                logo          : result.logo
                                            )
            viewModel = DetailedPromo.ShowPromo.ViewModel(state: .result(mapped))
        }
        viewController.displayDetailedPromo(viewModel: viewModel)
    }
}
