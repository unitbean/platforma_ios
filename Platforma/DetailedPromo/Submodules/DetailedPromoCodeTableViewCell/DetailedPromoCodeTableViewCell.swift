//
//  DetailedPromoCodeTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 10.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedPromoCodeTableViewCell: UITableViewCell {
    
    var viewModel: DetailedPromoViewModel? {
        didSet {
            if let promoCode = viewModel?.promoCode, promoCode != "" {
                innerView.isHidden = false
                promoCodeLabel.text = viewModel?.promoCode
            } else {
                innerView.isHidden = true
                promoCodeLabel.text = viewModel?.promoCode
            }
        }
    }

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var promoCodeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    private func configureCell() {
        innerView.layer.cornerRadius = 8
    }
    
}
