//
//  DetailedPromoDetailedPromoTableViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

protocol DetailedPromoTableViewDataSourceDelegate: class {

}

class DetailedPromoTableViewDataSource: NSObject, UITableViewDataSource {

    var viewModel: DetailedPromoViewModel?

    let generator = UIImpactFeedbackGenerator(style: .heavy)
    private var makeTapticFeedback: Bool = true
    private var canBeDismissed = true
    fileprivate var possibleOffset: CGFloat {
        //MARK: - REFACTOR
        if UIDevice.current.deviceModel == .iPhone6 || UIDevice.current.deviceModel == .iPhone6S || UIDevice.current.deviceModel == .iPhone7 || UIDevice.current.deviceModel == .iPhone8 || UIDevice.current.deviceModel == .iPhone5S || UIDevice.current.deviceModel == .iPhoneSE {
            return -120
        } else {
            return -160
        }
    }
    
    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc:DetailedPromoViewController!
    /// Используется для сообщения ViewController о взаимодействии с tableView
    weak var delegate: DetailedPromoTableViewDataSourceDelegate?
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailedPromoDescriptionTableViewCell.self), for: indexPath) as! DetailedPromoDescriptionTableViewCell
            cell.viewModel = viewModel
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailedPromoCodeTableViewCell.self), for: indexPath) as! DetailedPromoCodeTableViewCell
            cell.viewModel = viewModel
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - UITableViewDelegate
extension DetailedPromoTableViewDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 200 && !vc.closeButtonIsHidden {
            vc.hideCloseButton()
        } else if scrollView.contentOffset.y < 200 && vc.closeButtonIsHidden {
            vc.showCloseButton()
        } else if scrollView.contentOffset.y <= possibleOffset {
            
            switch scrollView.panGestureRecognizer.state {
            case .began:
                makeTapticFeedback = true
            case .changed:
                if makeTapticFeedback {
                    makeTapticFeedback = false
                    generator.prepare()
                    generator.impactOccurred()
                }
            case .possible:
                if makeTapticFeedback {
                    makeTapticFeedback = false
                    generator.prepare()
                    generator.impactOccurred()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    if self.canBeDismissed == true {
                        self.canBeDismissed = false
                        self.vc.presentingViewController?.dismiss(animated: true, completion: nil)
                    }
                }
            default: break
            }
        } 
    }
}














