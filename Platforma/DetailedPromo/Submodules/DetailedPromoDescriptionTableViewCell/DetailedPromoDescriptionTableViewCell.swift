//
//  DetailedPromoDescriptionTableViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedPromoDescriptionTableViewCell: UITableViewCell {

    var viewModel: DetailedPromoViewModel? {
        didSet {
            if let description = viewModel?.description {
                descriptionLabel.text = description
            } else {
                descriptionLabel.text = ""
            }
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
