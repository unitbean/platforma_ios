//
//  DetailedPromoDetailedPromoDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum DetailedPromo {
    // MARK: Use cases
    enum ShowPromo {
        struct Request {
        }

        struct Response {
            var result: DetailedPromo.RequestResult
        }

        struct ViewModel {
            var state: ViewControllerState
        }
    }

    enum ViewControllerState {
        case loading
        case result(DetailedPromoViewModel)
        case emptyResult
        case error(message: String)
    }

    enum RequestResult {
        case failure(String)
        case success(PartnerSaleModel)
    }
}


