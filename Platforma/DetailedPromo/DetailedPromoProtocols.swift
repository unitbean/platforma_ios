//
//  DetailedPromoDetailedPromoProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// DetailedPromo DataProvider Interface
protocol DetailedPromoDataProviderInput {
    func getDetailedPromo(completion: @escaping (PartnerSaleModel?, String?) -> Void)
}

/// DetailedPromo Router Interface
protocol DetailedPromoRouterProtocol {
    func routeToPartnerSales(fromVc: DetailedPromoViewController)
}

/// DetailedPromo ViewController Interface
protocol DetailedPromoViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayDetailedPromo(viewModel: DetailedPromo.ShowPromo.ViewModel)
}

/// DetailedPromo View Interface
protocol DetailedPromoViewInput: class {
    func displayNoResult()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateTableViewData()
}

/// DetailedPromo Interactor Interface
protocol DetailedPromoInteractorInput {
    func loadDetailedPromo(request: DetailedPromo.ShowPromo.Request)
}

/// DetailedPromo Presenter Interface
protocol DetailedPromoPresenterInput: class {
    func presentDetailedPromo(response: DetailedPromo.ShowPromo.Response)
}






