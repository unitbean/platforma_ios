//
//  DetailedPromoDetailedPromoViewModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct DetailedPromoViewModel {
    var title       : String?
    var description : String?
    var text        : String?
    var promoCode   : String?
    var pic         : String?
    var logo        : String?
}
