//
//  DetailedPromoDetailedPromoDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля DetailedPromo
struct DetailedPromoDataProvider: DetailedPromoDataProviderInput {
    
    let dataStore: DetailedPromoDataStore

    init(dataStore: DetailedPromoDataStore) {
        self.dataStore = dataStore
    }

    
    // Предоставление данных согласно определенным Use cases

    func getDetailedPromo(completion: @escaping (PartnerSaleModel?, String?) -> Void) {
        if let model = dataStore.promoModel {
            return completion(model, nil)
        }
    }
}
