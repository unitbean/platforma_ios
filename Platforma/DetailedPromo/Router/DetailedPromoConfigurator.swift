//
//  DetailedPromoDetailedPromoConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class DetailedPromoConfigurator {

    func configureModule(viewController: DetailedPromoViewController) {
        let dataStore = DIAssembly.instance().dataStoresList.detailedPromoDataStore
        let presenter = DetailedPromoPresenter(viewController: viewController)
        let dataProvider = DetailedPromoDataProvider(dataStore: dataStore)
        let interactor = DetailedPromoInteractor(dataProvider: dataProvider, presenter: presenter)
        let router = DetailedPromoRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
