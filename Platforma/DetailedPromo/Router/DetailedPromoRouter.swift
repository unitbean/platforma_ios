//
//  DetailedPromoDetailedPromoRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class DetailedPromoRouter: DetailedPromoRouterProtocol {

    var dataStoresList: /*App name*/DataStoresList

    func routeToPartnerSales/*AnyViewController*/(fromVc: DetailedPromoViewController) {
        // Осуществить добавление ViewController в stack
        // ...

        // При необходимости осуществить трансфер данных
    }




    init(dataStoresList: /*App name*/DataStoresList) {
        self.dataStoresList = dataStoresList
    }
}
