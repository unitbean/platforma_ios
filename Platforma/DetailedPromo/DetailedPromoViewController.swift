//
//  DetailedPromoDetailedPromoViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class DetailedPromoViewController: UIViewController {

    @IBOutlet var tableView: DetailedEventTableView!
    @IBOutlet weak var tableViewHeaderImageView: UIImageView!
    @IBOutlet var headerImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var logoBorderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewText: UILabel!
    @IBOutlet weak var gradientView: UIView!
    
    let tableViewDataSource = DetailedPromoTableViewDataSource()
    let cellIdentifiers = [String(describing: DetailedPromoDescriptionTableViewCell.self),
                           String(describing: DetailedPromoCodeTableViewCell.self)]
    
    let configurator = DetailedPromoConfigurator()
    
    var interactor: DetailedPromoInteractorInput!
    var router: DetailedPromoRouterProtocol!
    
    weak var delegate: DetailedEventViewControllerDelegate?
    
    var state = DetailedPromo.ViewControllerState.loading
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var closeButtonIsHidden: Bool = false

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configureModule(viewController: self)
        setupSubViews()
        displayInitial()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func closeButtonDidTouch(_ sender: Any) {
        DispatchQueue.main.async {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            self.delegate?.updateCards()
        }
    }
    
    //MARK: - Private methods
    private func setupTableView() {
        for identifier in cellIdentifiers {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDataSource
        tableViewDataSource.vc = self
        tableView.tableHeaderView?.frame.size.height = UIScreen.main.bounds.height / 3
        headerImageViewHeight.constant = UIScreen.main.bounds.height / 3
        DispatchQueue.main.async {
            [weak self] in
            self?.tableView.layoutIfNeeded()
        }
    }
    
    private func setupCloseButton() {
        closeButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        closeButton.alpha = 0
    }
    
    private func setupSubViews() {
        setupTableView()
        setupCloseButton()
    }
    
    private func displayInitial() {
        showProgress()
        interactor.loadDetailedPromo(request: DetailedPromo.ShowPromo.Request())
    }
}

//MARK: - DetailedPromoViewControllerInput
extension DetailedPromoViewController: DetailedPromoViewControllerInput {
    func displayDetailedPromo(viewModel: DetailedPromo.ShowPromo.ViewModel) {
        state = viewModel.state
        switch state {
        case let .error(message):
            print("error \(message)")
        case let .result(item):
            hideProgress()
            tableViewDataSource.viewModel = item
            setupTableViewSubViews(viewModel: item)
            if let promoPic = item.pic {
                tableViewHeaderImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + promoPic))
            }
            updateTableViewData()
        case .emptyResult:
            print("empty result")
        default: break
        }
    }
}

//MARK: - DetailedPromoViewInput
extension DetailedPromoViewController: DetailedPromoViewInput {

    func showTable() {
        // TODO: - Отображение tableView
    }
    
    func setupTableViewSubViews(viewModel: DetailedPromoViewModel) {
        logoImageView.layer.masksToBounds = true
        logoBorderView.layer.cornerRadius = logoBorderView.frame.width / 2
        logoImageView.layer.cornerRadius = logoImageView.frame.width / 2
        
        if let pic = viewModel.pic {
            tableViewHeaderImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + pic))
        }
        if let logo = viewModel.logo {
            logoImageView.kf.setImage(with: URL(string: GlobalConstants.picHost + logo))
        }
        if let title = viewModel.title, let preview = viewModel.text {
            titleLabel.text = title
            previewText.text = preview
        }
    }
    
    func showCloseButton() {
        closeButtonIsHidden = false
        closeButton.isHidden = closeButtonIsHidden
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: .curveEaseInOut, animations: { [weak self] in
            self?.closeButton.alpha = 1
            self?.closeButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self?.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }, completion: nil)
        
        UIView.animate(withDuration: 0.4, delay: 0.15, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.6, options: UIViewAnimationOptions.curveEaseIn, animations: { [weak self] in
            self?.closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 4)
            }, completion: nil)
    }
    
    func hideCloseButton() {
        closeButtonIsHidden = true
        UIView.animate(withDuration: 0.2, animations: {
            [weak self] in
            self?.closeButton.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            self?.closeButton.alpha = 0
        }) {_ in
            self.closeButton.isHidden = self.closeButtonIsHidden
        }
    }

    func displayNoResult() {
        // TODO: - Отображение отсутствия результата
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        // TODO: - Отображение ошибки соединения
    }

    func hideConnectionError() {
        // TODO: - Скрыть отображение ошибки соединения
    }

    func showProgress() {
        // TODO: - Показать прогресс-бар
    }

    func hideProgress() {
        // TODO: - Скрыть прогресс-бар
    }

    func updateTableViewData() {
        showTable()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}














