//
//  DetailedPromoDetailedPromoInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 09/11/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// Класс для описания бизнес-логики модуля DetailedPromo
class DetailedPromoInteractor: DetailedPromoInteractorInput {

    let presenter: DetailedPromoPresenterInput
    let dataProvider: DetailedPromoDataProviderInput

    required init(dataProvider: DetailedPromoDataProvider, presenter: DetailedPromoPresenter) {
        self.dataProvider = dataProvider
        self.presenter = presenter
    }

    
    
    func loadDetailedPromo(request: DetailedPromo.ShowPromo.Request) {
        dataProvider.getDetailedPromo { [unowned self] (model, error) in
            let result: DetailedPromo.RequestResult
            if let model = model {
                result = .success(model)
            } else if let error = error {
                result = .failure(error)
            } else {
                result = .failure("Ошибка получения данных")
            }
            self.presenter.presentDetailedPromo(response: DetailedPromo.ShowPromo.Response(result: result))
        }
    }
}
