//
//  EventCardCollectionViewCell.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 01.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit


protocol EventCardCollectionViewCellTransitionDelegate: class {
    func routeToSubPurchase()
}


class EventCardCollectionViewCell: UICollectionViewCell, CardCollectionViewCellProtocol {

    @IBOutlet weak var topCardContainerView: UIView!
    @IBOutlet weak var backgroundFeelerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomCardContainerView: UIView!
    @IBOutlet weak var titleContainerViewToTopCardConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var orderSubButton: UIButton!
    
    var initialFrame: CGRect?
    var initialCornerRadius: CGFloat?
    var bottomCardContainerViewConstraint: NSLayoutConstraint?
    var gradientColors = [ColorConstants.adOrangeGradientColor,
                          ColorConstants.adVioletGradientColor,
                          ColorConstants.adBlueGradientColor]
    
    weak var delegate: EventCardCollectionViewCellTransitionDelegate?

    var viewModel: EventViewModel? {
        didSet {
            if viewModel?.isAd == true {
                adView.isHidden = false
                topCardContainerView.isHidden = true
                titleContainerView.isHidden = true
                bottomCardContainerView.isHidden = true
                adView.backgroundColor = ColorConstants.adOrangeGradientColor
                addGradient()
            } else {
                adView.isHidden = true
                topCardContainerView.isHidden = false
                titleContainerView.isHidden = false
                bottomCardContainerView.isHidden = false
                typeLabel.text = viewModel?.type
                titleLabel.text = viewModel?.title
                if PlatformaUserDefaults.getUserPremiumStatus() == true, viewModel?.freeForPremium == true {
                    priceLabel.text = "FREE"
                    priceLabel.textColor = UIColor.white
                    priceView.backgroundColor = ColorConstants.pink
                    dateTimeLabel.text = viewModel?.startDate
                    placeLabel.text = viewModel?.placeTitle
                } else if let price = viewModel?.price {
                    priceLabel.text = String(describing: price) + " ₽"
                    priceLabel.textColor = UIColor.black
                    priceView.backgroundColor = ColorConstants.cyan
                    dateTimeLabel.text = viewModel?.startDate
                    placeLabel.text = viewModel?.placeTitle
                } else {
                    priceLabel.text = ""
                }
                    
                if viewModel?.price == 0 {
                    priceView.isHidden = true
                } else {
                    priceView.isHidden = false
                }
            }
        }
    }
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureAll()
        bottomCardContainerViewConstraint = bottomCardContainerView.heightAnchor.constraint(equalToConstant: 154.5)
    }
    
    @IBAction func orderSubButtonDidTouch(_ sender: Any) {
        delegate?.routeToSubPurchase()
    }
    
    
    // MARK: - Configuration
    private func configureAll() {
        configureCell()
        configureSubviews()
    }
    
    private func configureSubviews() {
        priceView.layer.cornerRadius = 7
        priceView.layer.masksToBounds = true
        orderSubButton.layer.cornerRadius = 10
    }
    
    private func addGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = adView.bounds
        gradient.colors = [ColorConstants.adOrangeGradientColor.cgColor,
                           ColorConstants.adVioletGradientColor.cgColor,
                           ColorConstants.adBlueGradientColor.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        adView.layer.insertSublayer(gradient, at: 0)
    }
    
    // MARK: - Expanding/Collapsing Logic
    func expand(in collectionView: UICollectionView) {
        initialFrame = self.frame
        initialCornerRadius = self.contentView.layer.cornerRadius
        
        self.contentView.layer.cornerRadius = 0
        
        if UIDevice.current.deviceModel == .iPhoneX {
            self.frame = CGRect(x: 0, y: collectionView.contentOffset.y - 44, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 2 + titleContainerView.frame.height + 88)
        } else {
            self.frame = CGRect(x: 0, y: collectionView.contentOffset.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 2 + titleContainerView.frame.height)
        }
  
        bottomCardContainerView.alpha = 0
        separatorView.isHidden = true
        
        bottomCardContainerView.translatesAutoresizingMaskIntoConstraints = false
        bottomCardContainerViewConstraint?.isActive = true
        titleContainerViewToTopCardConstraint.constant += titleContainerView.frame.height
        
        backgroundFeelerView.isHidden = true
        
        layoutIfNeeded()
    }
    
    func collapse() {
        self.contentView.layer.cornerRadius = initialCornerRadius ?? self.contentView.layer.cornerRadius
        self.frame = initialFrame ?? self.frame
        
        initialFrame = nil
        initialCornerRadius = nil
        
        bottomCardContainerView.alpha = 1
        separatorView.isHidden = false
        
        bottomCardContainerView.translatesAutoresizingMaskIntoConstraints = false
        bottomCardContainerViewConstraint?.isActive = false
        titleContainerViewToTopCardConstraint.constant = 0
        
        backgroundFeelerView.isHidden = false
        
        layoutIfNeeded()
    }
    
    // MARK: - Showing/Hiding Logic
    func hide(in collectionView: UICollectionView, frameOfSelectedCell: CGRect) {
        initialFrame = self.frame
        
        let currentY = self.frame.origin.y
        let newY: CGFloat
        
        if currentY < frameOfSelectedCell.origin.y {
            let offset = frameOfSelectedCell.origin.y - currentY
            newY = collectionView.contentOffset.y - offset
        } else {
            let offset = currentY - frameOfSelectedCell.maxY
            newY = collectionView.contentOffset.y + collectionView.frame.height + offset
        }
        
        self.frame.origin.y = newY
        
        layoutIfNeeded()
    }
    
    func show() {
        self.frame = initialFrame ?? self.frame
        
        initialFrame = nil

        layoutIfNeeded()
    }
    
    
}
