//
//  MainScreenMainScreenCollectionViewDataSource.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit
import Kingfisher



protocol MainScreenCollectionViewDataSourceDelegate: class {
    func routeToDetailedEvent(eventIndex: Int)
}

class MainScreenCollectionViewDataSource: NSObject {
    
    var viewModels: [EventViewModel] = []
    /// Используется для делегирования действия ячеек напрямую к ViewController.
    weak var vc: MainScreenViewController!
    /// Используется для сообщения ViewController о взаимодействии с tableView
    weak var delegate: MainScreenCollectionViewDataSourceDelegate?
    
    private var expandedCell: EventCardCollectionViewCell?
    private var hiddenCells: [EventCardCollectionViewCell] = []
    private var adCellsCount: Int {
        let adCellsArray = viewModels.filter{ $0.isAd == true }
        return adCellsArray.count
    }
}

//MARK: - UICollectionViewDataSource
extension MainScreenCollectionViewDataSource: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: EventCardCollectionViewCell.self), for: indexPath) as! EventCardCollectionViewCell
        cell.delegate = vc
        cell.viewModel = viewModels[indexPath.row]
        if let eventPic = viewModels[indexPath.row].eventPic,
            let picUrl = URL(string: GlobalConstants.picHost + eventPic) {
            cell.imageView.kf.setImage(with: picUrl)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerIdentifier = String(describing: CardHeaderCollectionReusableView.self)
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier, for: indexPath)
        
        headerView.frame.size.height = 50
        
        return headerView
    }
}

//MARK: - UICollectionViewDelegate
extension MainScreenCollectionViewDataSource: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if viewModels[indexPath.row].isAd != true {
          delegate?.routeToDetailedEvent(eventIndex: indexPath.row)
        } 
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension MainScreenCollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: 485)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 335, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 1.0, bottom: 1.0, right: 1.0)
    }
}

















