//
//  MainScreenMainScreenInteractor.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

/// Класс для описания бизнес-логики модуля MainScreen
class MainScreenInteractor: MainScreenInteractorInput {

    let presenter: MainScreenPresenterInput
    let dataProvider: MainScreenDataProviderInput
    
    private let eventsAdModifierWorker = EventsAdModifierWorker()
    

    required init(dataProvider: MainScreenDataProvider, presenter: MainScreenPresenter) {
        self.dataProvider = dataProvider
        self.presenter = presenter
    }

    func loadEvents(request: MainScreen.Events.Request) {
        dataProvider.getEvents { [weak self] (model, error) in
            guard let self = self else { return }
            let result: MainScreen.Events.RequestResult
            if let model = model {
                // premium user logic
                if PlatformaUserDefaults.getUserPremiumStatus() != true {
                    // Modifying model with ads for non-premium user
                    let modifiedModel = self.eventsAdModifierWorker.modifyModelWithAds(from: model)
                    result = .success(modifiedModel)
                    
                    self.dataProvider.saveAds(model: modifiedModel)
                } else {
                    result = .success(model)
                }
            } else if let error = error {
                result = .failure(error)
            } else {
                result = .failure("Ошибка получения данных")
            }
            self.presenter.presentEvents(response: MainScreen.Events.Response(result: result))
        }
    }
}
