//
//  MainScreenMainScreenDataProvider.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


/// Отвечает за получение данных модуля MainScreen
struct MainScreenDataProvider: MainScreenDataProviderInput {
    let dataStore: MainScreenDataStore
    let apiService: MainScreenAPIServiceProtocol

    init(dataStore: MainScreenDataStore, apiService: MainScreenAPIServiceProtocol) {
        self.dataStore = dataStore
        self.apiService = apiService
    }

    // Предоставление данных согласно определенным Use cases

    func getEvents(completion: @escaping (MainScreenModel?, String?) -> Void) {
        apiService.getEvents { (model, error) in
            if let error = error {
                completion(nil, error)
            } else if let model = model {
                self.dataStore.mainModel = model
                completion(self.dataStore.mainModel, nil)
            }
        }
    }
    
    func saveAds(model: MainScreenModel) {
        dataStore.mainModel = model
    }
}
