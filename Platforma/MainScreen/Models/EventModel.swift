//
//  EventModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 31.10.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


struct EventModel: Codable, Equatable {
    var id              : String?
    var typeTitle       : String?
    var title           : String?
    var startDate       : Double?
    var placeTitle      : String?
    var previewPic      : String?
    var mainPic         : String?
    var previewText     : String?
    var description     : String?
    var freeForPremium  : Bool?
    var price           : Int?
    var linkToRegister  : String?
    var status          : String?
    var isAd            : Bool?
    
    static func == (lhs: EventModel, rhs: EventModel) -> Bool {
        return lhs.id == rhs.id && lhs.title == rhs.title
    }
}
