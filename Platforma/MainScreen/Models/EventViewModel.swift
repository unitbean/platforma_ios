//
//  MainScreenMainScreenViewModel.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

struct EventViewModel {
    var type            : String?
    var title           : String?
    var startDate       : String?
    var placeTitle      : String?
    var eventPic        : String?
    var freeForPremium  : Bool?
    var price           : Int?
    var status          : String?
    var isAd            : Bool?
}
