//
//  MainScreenMainScreenProtocols.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation
import UIKit

/// MainScreen DataProvider Interface
protocol MainScreenDataProviderInput {
    func getEvents(completion: @escaping (MainScreenModel?, String?) -> Void)
    func saveAds(model: MainScreenModel)
}

/// MainScreen Router Interface
protocol MainScreenRouterProtocol {
    func routeToDetailedEvent(fromVc: MainScreenViewController, modelIndex: Int)
    func routeToSubPurchase(fromVc: MainScreenViewController)
}

/// MainScreen ViewController Interface
protocol MainScreenViewControllerInput: class {

    // TODO: - Изменить название Use case
    func displayEvents(viewModel: MainScreen.Events.ViewModel)
}

/// MainScreen View Interface
protocol MainScreenViewInput: class {
    func displayNoResult()
    func displayError(error: String)
    func displayConnectionError()
    func hideConnectionError()
    func showProgress()
    func hideProgress()
    func updateCollectionViewData(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource)
}

/// MainScreen Interactor Interface
protocol MainScreenInteractorInput {
    func loadEvents(request: MainScreen.Events.Request)
}

/// MainScreen Presenter Interface
protocol MainScreenPresenterInput: class {
    func presentEvents(response: MainScreen.Events.Response)
}






