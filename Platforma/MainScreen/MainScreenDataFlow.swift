//
//  MainScreenMainScreenDataFlow.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

enum MainScreen {
    // MARK: Use cases
    enum Events {
        struct Request {
        }

        struct Response {
            var result: MainScreen.Events.RequestResult
        }

        struct ViewModel {
            var state: ViewControllerState
        }
        
        enum RequestResult {
            case failure(String)
            case success(MainScreenModel)
        }
        
        enum ViewControllerState {
            case loading
            case result([EventViewModel])
            case emptyResult
            case error(message: String)
        }
    }
}


