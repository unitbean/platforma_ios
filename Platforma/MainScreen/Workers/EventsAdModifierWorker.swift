//
//  EventsAdModifierWorker.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation


class EventsAdModifierWorker {
    
    func modifyModelWithAds(from model: MainScreenModel) -> MainScreenModel {
        if let events = model.events {
            let modifiedEvents = insertAdsInEvents(events: events)
            let newModel = MainScreenModel(events: modifiedEvents)
            return newModel
        } else {
            return model
        }
    }
    
    private func insertAdsInEvents(events: [EventModel]) -> [EventModel]? {
        let adModel = EventModel(id: nil, typeTitle: nil, title: nil, startDate: nil, placeTitle: nil, previewPic: nil, mainPic: nil, previewText: nil, description: nil, freeForPremium: nil, price: nil, linkToRegister: nil, status: nil, isAd: true)
        var newEventsArray = events
        
        var countOfAds = 0          // количество добавленных рекламных вставок
        
        for index in 0..<events.count {
            let modifiedIndex = index + countOfAds
            if index % 4 == 0 && index > 0 {
                newEventsArray.insert(adModel, at: modifiedIndex)
                countOfAds += 1
            }
        }
        return newEventsArray
    }
}
