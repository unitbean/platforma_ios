//
//  MainScreenMainScreenPresenter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import Foundation

class MainScreenPresenter: MainScreenPresenterInput {

    weak var viewController: MainScreenViewControllerInput?

    required init(viewController: MainScreenViewController) {
        self.viewController = viewController
    }

    func presentEvents(response: MainScreen.Events.Response) {
        var viewModel = MainScreen.Events.ViewModel(state: .emptyResult)

        switch response.result {
        case let .failure(error):
            viewModel = MainScreen.Events.ViewModel(state: .error(message: error))
        case let .success(result):
            if let events = result.events {
                if events.isEmpty {
                    viewModel = MainScreen.Events.ViewModel(state: .emptyResult)
                } else {
                    let timeFormatter = TimeFormatWorker()
                    let mapped = result.events!.map {
                        EventViewModel(type               : $0.typeTitle,
                                       title            : $0.title,
                                       startDate        : timeFormatter.convertMillsToDateString($0.startDate, dateFormat: "dd.MM / HH:mm"),
                                       placeTitle       : $0.placeTitle,
                                       eventPic         : $0.mainPic,
                                       freeForPremium   : $0.freeForPremium,
                                       price            : $0.price,
                                       status           : $0.status,
                                       isAd             : $0.isAd
                                       )
                    }
                    viewModel = MainScreen.Events.ViewModel(state: .result(mapped))
                }
            }
        }
        viewController?.displayEvents(viewModel: viewModel)
    }
}
