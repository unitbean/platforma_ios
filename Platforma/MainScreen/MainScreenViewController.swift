//
//  MainScreenMainScreenViewController.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {

    @IBOutlet weak var connectionErrorPlaceholderView: UIView!
    @IBOutlet weak var connectionErrorImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let configurator = MainScreenConfigurator()
    var interactor: MainScreenInteractorInput!
    var router: MainScreenRouterProtocol!
    
    var isStatusBarHidden = false
    
    override var prefersStatusBarHidden: Bool {
        return isStatusBarHidden
    }
    
    let transition = CardPresentingAnimator(duration: 0.58)
    
    fileprivate let collectionViewDataSource = MainScreenCollectionViewDataSource()
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate let activityIndicator = ActivityIndicator()
    private var isPremium: Bool? {
        willSet {
            if isPremium != newValue {
                refresh()
            }
        }
    }
    private var state = MainScreen.Events.ViewControllerState.loading
    
    
    
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configureModule(viewController: self)
        collectionView.register(UINib(nibName: String(describing: EventCardCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: EventCardCollectionViewCell.self))
        collectionView.register(UINib(nibName: "CardHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String(describing: CardHeaderCollectionReusableView.self))
        collectionView.contentInset = UIEdgeInsetsMake(10, 0, 20, 0)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        setupConnectionErrorView()
        activityIndicator.setup(with: self.view, backgroundColor: UIColor.white)
        display(newState: state)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.isPremium = PlatformaUserDefaults.getUserPremiumStatus()
    }
    
    //Private methods
    @objc private func refresh() {
        connectionErrorPlaceholderView.isHidden = true
        interactor.loadEvents(request: MainScreen.Events.Request())
    }
    
    private func setupConnectionErrorView() {
        connectionErrorImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageDidTap)))
        connectionErrorImageView.isUserInteractionEnabled = true
    }
    
    @objc private func imageDidTap() {
        refresh()
    }
}

//MARK: - MainScreenViewControllerInput
extension MainScreenViewController: MainScreenViewControllerInput {
    func displayEvents(viewModel: MainScreen.Events.ViewModel) {
        display(newState: viewModel.state)
    }

    func display(newState: MainScreen.Events.ViewControllerState) {
        state = newState
        switch state {
        case .loading:
            hideConnectionError()
            showProgress()
            interactor.loadEvents(request: MainScreen.Events.Request())
        case let .error(message):
            hideProgress()
            displayConnectionError()
        case let .result(eventViewModels):
            hideConnectionError()
            hideProgress()
            collectionViewDataSource.viewModels = eventViewModels
            updateCollectionViewData(delegate: collectionViewDataSource, dataSource: collectionViewDataSource)
        case .emptyResult:
            displayConnectionError()
        }
    }
}

//MARK: - MainScreenViewInput
extension MainScreenViewController: MainScreenViewInput {

    func showCollection() {
        collectionView.isHidden = false
    }

    func displayNoResult() {
        // TODO: - Отображение отсутствия результата
    }

    func displayError(error: String) {
        // TODO: - Отображение ошибки
    }

    func displayConnectionError() {
        connectionErrorPlaceholderView.isHidden = false
        collectionView.isHidden = true
    }

    func hideConnectionError() {
        connectionErrorPlaceholderView.isHidden = true
    }

    func showProgress() {
        activityIndicator.show()
    }

    func hideProgress() {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        activityIndicator.hide()
    }

    func updateCollectionViewData(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        DispatchQueue.main.async {
            self.showCollection()
            self.collectionViewDataSource.vc = self
            self.collectionViewDataSource.delegate = self
            self.collectionView.dataSource = dataSource
            self.collectionView.delegate = delegate
            self.collectionView.reloadSections([0])
        }
    }
}

//MARK: - MainScreenCollectionViewDataSourceDelegate
extension MainScreenViewController: MainScreenCollectionViewDataSourceDelegate {
    func routeToDetailedEvent(eventIndex: Int) {
        router.routeToDetailedEvent(fromVc: self, modelIndex: eventIndex)
    }
}

//MARK: - UIViewControllerTransitioningDelegate
extension MainScreenViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
}

//MARK: - EventCardCollectionViewCellTransitionDelegate
extension MainScreenViewController: EventCardCollectionViewCellTransitionDelegate {
    func routeToSubPurchase() {
        router.routeToSubPurchase(fromVc: self)
    }
}

//MARK: - SubPurchaseViewControllerDelegate
extension MainScreenViewController: SubPurchaseViewControllerDelegate {
    func reloadData() {
        display(newState: MainScreen.Events.ViewControllerState.loading)
    }
}

//MARK: - DetailedEventViewControllerDelegate
extension MainScreenViewController: DetailedEventViewControllerDelegate {
    func updateCards() {}
    
    func routeToSubscriptionPurchase() {
        router.routeToSubPurchase(fromVc: self)
    }
}
















