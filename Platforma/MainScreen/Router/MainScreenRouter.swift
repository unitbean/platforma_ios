//
//  MainScreenMainScreenRouter.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import UIKit

/// Класс, ответственный за роутинг между экранами
class MainScreenRouter: MainScreenRouterProtocol {

    var dataStoresList: DataStoresList?

    func routeToDetailedEvent(fromVc: MainScreenViewController, modelIndex: Int) {
        dataStoresList?.detailedEventDataStore.eventModel = dataStoresList?.mainScreenDataStore.mainModel?.events?[modelIndex]
        
        let toVc = UIStoryboard(name: "DetailedEvent", bundle: nil).instantiateInitialViewController() as! DetailedEventViewController
        toVc.transitioningDelegate = fromVc
        toVc.delegate = fromVc
        fromVc.transition.delegate = toVc
        fromVc.present(toVc, animated: true, completion: nil)
    }




    init(dataStoresList: DataStoresList) {
        self.dataStoresList = dataStoresList
    }
    
    func routeToSubPurchase(fromVc: MainScreenViewController) {
        let toVc = UIStoryboard(name: "SubPurchase", bundle: nil).instantiateInitialViewController() as! SubPurchaseViewController
        toVc.modalPresentationStyle = .custom
        toVc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        toVc.delegate = fromVc
        fromVc.navigationController?.present(toVc, animated: true, completion: nil)
    }
}
