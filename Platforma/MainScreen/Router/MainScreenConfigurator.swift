//
//  MainScreenMainScreenConfigurator.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 30/10/2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import EasyDi
import UIKit

class MainScreenConfigurator {

    func configureModule(viewController: MainScreenViewController) {
        let apiService = DIAssembly.instance().api
        let dataStore = DIAssembly.instance().dataStoresList.mainScreenDataStore
        let presenter = MainScreenPresenter(viewController: viewController)
        let dataProvider = MainScreenDataProvider(dataStore: dataStore, apiService: apiService)
        let interactor = MainScreenInteractor(dataProvider: dataProvider, presenter: presenter)
        let router = MainScreenRouter(dataStoresList: DIAssembly.instance().dataStoresList)

        viewController.interactor = interactor
        viewController.router = router
    }
}
