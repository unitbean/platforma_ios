//
//  ChoosenEventsPresenterTests.swift
//  PlatformaTests
//
//  Created by Levan Chikvaidze on 30.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import XCTest

@testable import Platforma

class ChoosenEventsPresenterTests: XCTestCase {
    
    var choosenEventsPresenter: ChoosenEventsPresenter!
    var modelMock: EventModel!

    override func setUp() {
        super.setUp()
        setupModelMock()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func setupModelMock() {
        let eventModel = EventModel(id: "", typeTitle: "", title: "", startDate: 12341521, placeTitle: nil, previewPic: nil, mainPic: nil, previewText: nil, description: nil, freeForPremium: nil, price: nil, linkToRegister: nil, status: nil, isAd: nil)
        modelMock = eventModel
    }
    
    
    //MARK: - Dependency spy
    class ChoosenEventsViewControllerInterfaceSpy: ChoosenEventsViewControllerInput {
        var displayChoosenEventsCalled = false
        var startDate: String?
        
        func displayChoosenEvents(viewModel: ChoosenEvents.ShowEvents.ViewModel) {
            self.displayChoosenEventsCalled = true
            
            switch viewModel.state {
            case let .result(viewModels):
                self.startDate = viewModels[0].startDate
            default: break
            }
        }
    }
    
    //MARK: - Test ViewController call
    func testChoosenEventsPresenterShouldCallViewControllerToDisplayEvent() {
        // Given
        let choosenEventsViewControllerInterfaceSpy = ChoosenEventsViewControllerInterfaceSpy()
        choosenEventsPresenter = ChoosenEventsPresenter(viewController: choosenEventsViewControllerInterfaceSpy)
        
        let responseResult = ChoosenEvents.ShowEvents.RequestResult.success(ChoosenEventsModel(events: [modelMock]))
        let response = ChoosenEvents.ShowEvents.Response(result: responseResult)
        
        // When
        choosenEventsPresenter.presentChoosenEvents(response: response)
        
        // Then
        XCTAssert(choosenEventsViewControllerInterfaceSpy.displayChoosenEventsCalled, "Presenter should ask ViewController to display choosen events")
        XCTAssert(choosenEventsViewControllerInterfaceSpy.startDate != "", "TimeFormatter worker, used by Presenter, should convert millis to String")
    }
}
