//
//  ChoosenEventsInteractorTests.swift
//  Platforma
//
//  Created by Levan Chikvaidze on 29.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import XCTest

@testable import Platforma

class ChoosenEventsInteractorTests: XCTestCase {

    var choosenEventsInteractor: ChoosenEventsInteractor!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    //MARK: - DataProvider mock
    class ChoosenEventsDataProviderMock: ChoosenEventsDataProviderInput {
        
        func storeChoosenEvents(events: ChoosenEventsModel?) {}
    
        func getStoredData<T>() -> [T]? where T : Equatable {
            let storedEvents = [EventModel()]
            return storedEvents as? [T]
        }
        
        func getEvents(completion: @escaping ([EventModel]?, String?) -> Void) {
            let eventModel = EventModel(id: "", typeTitle: "", title: "", startDate: nil, placeTitle: nil, previewPic: nil, mainPic: nil, previewText: nil, description: nil, freeForPremium: nil, price: nil, linkToRegister: nil, status: nil, isAd: nil)
            completion([eventModel], nil)
        }
    }
    
    //MARK: - Dependency spy
    class ChoosenEventsPresenterInterfaceSpy: ChoosenEventsPresenterInput {
        var presentChoosenEventsCalled = false
        
        func presentChoosenEvents(response: ChoosenEvents.ShowEvents.Response) {
            presentChoosenEventsCalled = true
        }
    }
    
    //MARK: - Test presenter call
    func testChoosenEventsInteractorShouldCallPresenterToPresentChoosenEvents() {
        // Given
        let request = ChoosenEvents.ShowEvents.Request()
        let dataProvider = ChoosenEventsDataProviderMock()
        let choosenEventsPresenterInterfaceSpy = ChoosenEventsPresenterInterfaceSpy()
        choosenEventsInteractor = ChoosenEventsInteractor(dataProvider: dataProvider, presenter: choosenEventsPresenterInterfaceSpy)
        
        // When
        choosenEventsInteractor.loadChoosenEvents(request: request)
        
        // Then
        XCTAssert(choosenEventsPresenterInterfaceSpy.presentChoosenEventsCalled, "Should ask presenter")
    }
}
