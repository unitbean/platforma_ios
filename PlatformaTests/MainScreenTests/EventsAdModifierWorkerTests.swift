//
//  EventsAdModifierWorkerTests.swift
//  PlatformaTests
//
//  Created by Levan Chikvaidze on 30.11.2018.
//  Copyright © 2018 UnitBean. All rights reserved.
//

import XCTest

@testable import Platforma

class EventsAdModifierWorkerTests: XCTestCase {
    
    private var model: MainScreenModel!

    override func setUp() {
        super.setUp()
        setupModelMock()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    
    func setupModelMock() {
        let eventModel = EventModel(id: nil, typeTitle: nil, title: nil, startDate: nil, placeTitle: nil, previewPic: nil, mainPic: nil, previewText: nil, description: nil, freeForPremium: nil, price: nil, linkToRegister: nil, status: nil, isAd: nil)
        let model = MainScreenModel(events: [eventModel, eventModel, eventModel, eventModel, eventModel])
        self.model = model
    }
    
    func testWorkerModifiesEventModelWithAd() {
        // Given
        let eventsAdModifierWorker = EventsAdModifierWorker()
        guard let countBeforeModify = self.model.events?.count else { return }
        
        // When
        let enoughEvents = countBeforeModify > 4
        let model = eventsAdModifierWorker.modifyModelWithAds(from: self.model)
        
        // Then
        XCTAssert(enoughEvents && model.events?[4].isAd == true, "Worker should insert modified ad model for every 4th position in a row")
    }
}
